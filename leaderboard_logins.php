<div class="leaderboard_logins">
<?php

// Select leaderboard logins from user_stats DB //

$query = "SELECT * FROM user_stats ORDER BY total_logins DESC LIMIT 10";
$stmt = $dbh->prepare($query);
$stmt->execute();

// Fetch all of the remaining rows in the result set //

$result = $stmt->fetchAll();

// Display results from user_stats //

echo "<h3>Total Logins</h3>";

?>
<table>
<tbody>
<?php

$i = 0;

foreach( $result as $row ) {

        $i = $i + 1;
	$username = $row["username"];
	$logins = $row["total_logins"];
	echo '<tr>';
	echo '<td>';
	echo addOrdinalNumberSuffix($i);
	echo '</td>';
	echo '<td>';
	echo '<span><a href="user.php?user=';
	echo $username; 
	echo '">';
	echo $username;
	echo '</a>';
	echo "</span>";
	echo '</td>';
	echo '<td>';
	echo '<span> ';
	echo $logins;
	echo '</span>';
	echo '</td>';
	echo '</tr>';

}
?>

</tbody>
</table>
</div>