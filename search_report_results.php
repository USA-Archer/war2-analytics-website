<!DOCTYPE html >

<table>
<tbody>

<div class="recent_games">
<div class="recent_games_inner">
<h2 class="center">Filtered Game Results</h2>
<?php

echo '<tr>';
echo '<th>Name</th>';
echo '<th>Time</th>';
echo '<th>Map</th>';
echo '<th>Players</th>';
echo '</tr>';
echo '<tr>';



foreach( $result as $row ) {

	$name = $row["name"];
	$time = $row["gametime"];
	$map = $row["mapfile"];
	$id = $row["gameid"];
	$players = $row["players"];
	


	echo '<td>';
	echo '<span><a href="game_report.php?id=';
	echo $id; 
	echo '">';
	echo $name;
	echo '</a>';
	echo "</span>";
	echo '</td>';

	echo '<td>';
	echo sprintf('<span class="utcdt">%s</span></span>', str_replace('-','/',$time));
	echo '</td>';

	echo '<td>';
	echo $map;
	echo '</td>';
	
	echo '<td>';
	echo $players;
	echo '</td>';

	echo '</tr>';

}





?>
</div>
</tbody>
</table>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script>
function convertUTCDateToLocalDate(date) {
    var newDate = new Date(date.getTime()+date.getTimezoneOffset()*60*1000);
    var offset = date.getTimezoneOffset() / 60;
    var hours = date.getHours();
    newDate.setHours(hours - offset);
    return newDate;   
}


var x = document.getElementsByClassName("utcdt");
var i;
for (i = 0; i < x.length; i++) {
	var utcdt = x[i].innerHTML;
	var date = convertUTCDateToLocalDate(new Date(utcdt));
        x[i].innerHTML = date.toLocaleString();
    }

</script>
</body>

</html>