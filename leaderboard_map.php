

<div class="leaderboard_map">
<div class="map_table">
<table>
<tbody>
<?php
// Select leaderboard avg online from user_stats DB //

$query = "SELECT mapfile, COUNT(*) from game_info GROUP BY mapfile ORDER by COUNT(*) DESC LIMIT 50";
$stmt = $dbh->prepare($query);
$stmt->execute();

// Fetch all of the remaining rows in the result set //

$result = $stmt->fetchAll();

// Display results from user_stats //


$i = 0;

foreach( $result as $row ) {
	$i = $i + 1;
	$map = $row[0];
	$count = number_format($row[1]);
	echo '<tr>';
	echo '<td>';
	echo addOrdinalNumberSuffix($i);
	echo '</td>';
	echo '<td>';
	echo '<span>';
	echo $map;
	echo "</span>";
	echo '</td>';
	echo '<td>';
	echo '<span> ';
	echo $count;
	echo '</span>';
	echo '</td>';
	echo '</tr>';

}
?>

</tbody>

</table>
</div>
</div>
