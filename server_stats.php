

<?php

// Select user data from user_stats DB //

$query = "SELECT * FROM total_stats";
$stmt = $dbh->prepare($query);
$stmt->execute();

// Fetch all of the remaining rows in the result set //

$result = $stmt->fetchAll();

// Display results from total_stats //

if ($stmt->rowCount() > 0) {

foreach( $result as $row ) {

}
}
?>


<div class="all_users">

<div class="server_icon">
<img src="img/blue_user.png">
</div>

<div class="center">
<span class="bignum"><?php echo number_format($row["all_users"]); ?></span>

<br>
<h3>Users</h3>
</div>
</div>

<div class="all_logins">
<div class="server_icon">
<img src="img/blue_door.png">
</div>
<div class="center">
<span class="bignum"><?php echo number_format($row["all_logins"]); ?></span>
<br>
<h3>Logins</h3>
</div>
</div>



<div class="all_avg">
<div class="server_icon">
<img src="img/blue_clock.png">
</div>
<div class="center">
<span class="bignum"><?php echo minToTime($row["all_avg_online"]); ?></span>

<br>
<h3>Average Session</h3>
</div>


</div>

