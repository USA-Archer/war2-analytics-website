<div class="community">
<div class="community_wrapper">
<h2>Community</h2>
<div class="community_results">
<?php
// Select sum of users from country_stats DB //

$query = "SELECT SUM(number) FROM country_stats";
$stmt = $dbh->prepare($query);
$stmt->execute();

// Fetch all of the remaining rows in the result set //

$result = $stmt->fetchAll();

// Display results from country_stats //

foreach( $result as $row ) {

$sum_of_users = $row[0];

}

// Select country data from country_stats DB //

$query = "SELECT * FROM country_stats ORDER BY number DESC LIMIT 10";
$stmt = $dbh->prepare($query);
$stmt->execute();

// Fetch all of the remaining rows in the result set //

$result = $stmt->fetchAll();

// Display results from country_stats //

foreach( $result as $row ) {
$country = $row["country"];
$country_number = $row["number"];
$country_percent = round(100 * ($country_number/$sum_of_users));

echo '<img src="http://analytics.war2.co/icons/';
echo $country;
echo '.gif">';
echo '<span> ';
echo $country_percent;
echo '% - </span>';
echo " <span>";
echo strtoupper($country); 
echo "</span>";
echo '<span> - ';
echo $country_number;
echo '</span>';
echo "<br>";
echo "<br>";

}

?>
</div>
</div>
</div>