<!DOCTYPE html >

<head>
<meta name="viewport" content="initial-scale=1.0, user-scalable=yes" />
<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
<title>Warcraft 2 Analytics | War2.Co</title>
</head>
<?php 
echo '<a href="';
echo 'index.php';
echo '">Back to War2.Co</a>';
?>
<br>
<?php
require 'db_config.php';


try {
$dbh = new PDO("mysql:dbname=$nameofdb;host=localhost", $dbusername, $dbpassword);
$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} 

catch (PDOException $e) {
echo 'Connection failed: ' . $e->getMessage();
}

// Get id from GET //

$id = str_replace(array('%'),'',$_GET['id']); 

// If id not present, Quit //

if (!$id)
{
    exit;
}

// Select HACK! data from hack_history DB //

$query = "SELECT * FROM hack_history WHERE id LIKE :search";
$stmt = $dbh->prepare($query);
$stmt->bindValue(':search', $id, PDO::PARAM_INT);
$stmt->execute();

// Fetch all of the remaining rows in the result set //

$result = $stmt->fetchAll();

// Display results from hack_history //

if ($stmt->rowCount() > 0) {

foreach( $result as $row ) {

?>
<br>
<span>HACK!er: <a href="
<?php 
echo 'index.php?user=';
echo $row["username"];
echo '">';
echo $row["username"];
?></a></span>
<br>
<span>Time: <span class="utcdt"><?php echo str_replace('-','/',$row["gametime"]);?></span>
<br>
<span>Server: <?php echo $row["server"]; ?></span>
<br>
<br>
<?php
	$json = $row["proof"];
	$hack = json_decode($json);
	echo $hack->data_from_file;
	
 }
}
?>
<br>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script>
function convertUTCDateToLocalDate(date) {
    var newDate = new Date(date.getTime()+date.getTimezoneOffset()*60*1000);
    var offset = date.getTimezoneOffset() / 60;
    var hours = date.getHours();
    newDate.setHours(hours - offset);
    return newDate;   
}


var x = document.getElementsByClassName("utcdt");
var i;
for (i = 0; i < x.length; i++) {
	var utcdt = x[i].innerHTML;
	var date = convertUTCDateToLocalDate(new Date(utcdt));
        x[i].innerHTML = date.toLocaleString();
    }

</script>
</body>

</html>