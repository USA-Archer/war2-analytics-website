<?php

require 'db_config.php';


try {
$dbh = new PDO("mysql:dbname=$nameofdb;host=localhost", $dbusername, $dbpassword);
$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} 

catch (PDOException $e) {
echo 'Connection failed: ' . $e->getMessage();
}

$query = "SELECT * FROM country_stats where number != 0 ORDER BY number DESC";
$stmt = $dbh->prepare($query);
$stmt->execute();

// Fetch all of the remaining rows in the result set //

$result = $stmt->fetchAll();




  echo "{ \"cols\": [ {\"id\":\"\",\"label\":\"Country\",\"type\":\"string\"}, {\"id\":\"\",\"label\":\"Number\",\"type\":\"number\"}], \"rows\": [ ";
  $total_rows = $stmt->rowCount();
  $row_num = 0;
  foreach( $result as $row ){
    $row_num++;
    if ($row_num == $total_rows){

      echo "{\"c\":[{\"v\":\"";
      echo strtoupper($row['name']);



      echo "\",\"f\":null},{\"v\":" . $row['number'] . ",\"f\":null}]}";
    } else {
      echo "{\"c\":[{\"v\":\"";
      echo strtoupper($row['name']);
      echo "\",\"f\":null},{\"v\":" . $row['number'] . ",\"f\":null}]}, ";

     
    }
  }
  echo " ] }";

?>


