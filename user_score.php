<?php 


// Select user data from user_stats DB //

$query = "SELECT COUNT(*) FROM user_game_score WHERE username = :search";
$stmt = $dbh->prepare($query);
$stmt->bindValue(':search', $user, PDO::PARAM_INT);
$stmt->execute();

// Fetch all of the remaining rows in the result set //

$result = $stmt->fetchAll();
$row_count_user = $stmt->rowcount();



// Display 'username' Not Found. If no user is returned //

if ($row_count_user < 1) {

$user_game_count = "yes";

} else {
$user_game_count = 'none';

foreach( $result as $row ) {

$user_total_games = number_format($row[0]);
}
}

// Select user data from user_stats DB //

$query = "SELECT SUM(overall_score), SUM(units_produced), SUM(structures_constructed), SUM(gold_mined), SUM(lumber_harvested), SUM(oil_harvested), SUM(units_killed), SUM(structures_razed), SUM(min_in_game) FROM user_game_score WHERE username = :search";
$stmt = $dbh->prepare($query);
$stmt->bindValue(':search', $user, PDO::PARAM_INT);
$stmt->execute();

// Fetch all of the remaining rows in the result set //

$result = $stmt->fetchAll();
$row_count_user = $stmt->rowcount();



// Display 'username' Not Found. If no user is returned //

if ($row_count_user < 1) {

} else {


foreach( $result as $row ) {

$overall_score = number_format($row[0]);
$units = number_format($row[1]);
$buildings = number_format($row[2]);
$gold = number_format($row[3]);
$lumber = number_format($row[4]);
$oil = number_format($row[5]);
$kills = number_format($row[6]);
$razings = number_format($row[7]);
$game_time = minToTime($row[8]);


}
}






// Select user data from user_stats DB //

$query44 = "SELECT SUM(units_produced), username from user_game_score GROUP BY username ORDER BY SUM(units_produced) DESC limit 1";
$stmt44 = $dbh->prepare($query44);
$stmt44->execute();

// Fetch all of the remaining rows in the result set //

$result = $stmt44->fetchAll();

foreach( $result as $row ) {

$max_units = $row[0];

}



// BUILDINGS
// Select user data from user_stats DB //

$query44 = "SELECT SUM(structures_constructed), username from user_game_score GROUP BY username ORDER BY SUM(structures_constructed) DESC limit 1";
$stmt44 = $dbh->prepare($query44);
$stmt44->execute();

// Fetch all of the remaining rows in the result set //

$result = $stmt44->fetchAll();

foreach( $result as $row ) {

$max_buildings = $row[0];

}

// GOLD
// Select user data from user_stats DB //

$query44 = "SELECT SUM(gold_mined), username from user_game_score GROUP BY username ORDER BY SUM(gold_mined) DESC limit 1";
$stmt44 = $dbh->prepare($query44);
$stmt44->execute();

// Fetch all of the remaining rows in the result set //

$result = $stmt44->fetchAll();

foreach( $result as $row ) {

$max_gold = $row[0];

}

// LUMBER
// Select user data from user_stats DB //

$query44 = "SELECT SUM(lumber_harvested), username from user_game_score GROUP BY username ORDER BY SUM(lumber_harvested) DESC limit 1";
$stmt44 = $dbh->prepare($query44);
$stmt44->execute();

// Fetch all of the remaining rows in the result set //

$result = $stmt44->fetchAll();

foreach( $result as $row ) {

$max_lumber = $row[0];

}


// OIL
// Select user data from user_stats DB //

$query44 = "SELECT SUM(oil_harvested), username from user_game_score GROUP BY username ORDER BY SUM(oil_harvested) DESC limit 1";
$stmt44 = $dbh->prepare($query44);
$stmt44->execute();

// Fetch all of the remaining rows in the result set //

$result = $stmt44->fetchAll();

foreach( $result as $row ) {

$max_oil = $row[0];

}



// Kills
// Select user data from user_stats DB //

$query44 = "SELECT SUM(units_killed), username from user_game_score GROUP BY username ORDER BY SUM(units_killed) DESC limit 1";
$stmt44 = $dbh->prepare($query44);
$stmt44->execute();

// Fetch all of the remaining rows in the result set //

$result = $stmt44->fetchAll();

foreach( $result as $row ) {

$max_kills = $row[0];

}


// Razings
// Select user data from user_stats DB //

$query44 = "SELECT SUM(structures_razed), username from user_game_score GROUP BY username ORDER BY SUM(structures_razed) DESC limit 1";
$stmt44 = $dbh->prepare($query44);
$stmt44->execute();

// Fetch all of the remaining rows in the result set //

$result = $stmt44->fetchAll();

foreach( $result as $row ) {

$max_razings = $row[0];

}




if ($set_games != 'None') {
if ($user_game_count != 'yes') { 
require 'user_score_html.php';
} else {
}
} else {

}

?>