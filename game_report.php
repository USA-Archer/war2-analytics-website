<?php
require 'functions.php';
require 'db_config.php';


try {
$dbh = new PDO("mysql:dbname=$nameofdb;host=localhost", $dbusername, $dbpassword);
$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} 

catch (PDOException $e) {
echo 'Connection failed: ' . $e->getMessage();
}

// Get id from GET //

$id = str_replace(array('%'),'',$_GET['id']); 
$user_m = str_replace(array('%'),'',$_GET['user']); 



// If id not present, Quit //

if (!$id)
{
    exit;
}

if (!$user_m) {
// Select user with highest overall score from DB //

$query2 = "SELECT * FROM user_game_score WHERE gameid = :id ORDER BY overall_score DESC LIMIT 1";
$stmt2 = $dbh->prepare($query2);
$stmt2->bindValue(':id', $id, PDO::PARAM_INT);
$stmt2->execute();

// Fetch all of the remaining rows in the result set //

$result = $stmt2->fetchAll();

foreach( $result as $row ) {

$user_m = $row['username'];
$race_m = $row['race'];
$outcome_m = $row['outcome'];
$overall_score_m = $row['overall_score'];
$rank_m = war2Rank($race_m, $overall_score_m);

}



} else {

// Select user with highest overall score from DB //

$query2 = "SELECT * FROM user_game_score WHERE gameid = :id AND username = :user_m LIMIT 1";
$stmt2 = $dbh->prepare($query2);
$stmt2->bindValue(':id', $id, PDO::PARAM_INT);
$stmt2->bindValue(':user_m', $user_m, PDO::PARAM_INT);
$stmt2->execute();

// Fetch all of the remaining rows in the result set //

$result = $stmt2->fetchAll();

foreach( $result as $row ) {

$user_m = $row['username'];
$race_m = $row['race'];
$outcome_m = $row['outcome'];
$overall_score_m = $row['overall_score'];
$rank_m = war2Rank($race_m, $overall_score_m);

}

}

?>

<!DOCTYPE html >

<head>

<link rel="stylesheet" type="text/css" href="reset.css?ver=1">
<link rel="stylesheet" type="text/css" href="vanilla.css?ver=1">
<link rel="stylesheet" type="text/css" href="war2co.css?ver=109">
<meta name="viewport" content="initial-scale=1.0, user-scalable=yes" />
<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
<title>Warcraft 2 Analytics | War2.Co</title>
<style>
.hideme {
display: none;
}
html {
    cursor: url('img/<?php echo cursorImage($race_m); ?>'), default;
}

<?php
$i = 0;
// Select num of users in this game from DB //

$query2 = "SELECT * FROM user_game_score WHERE gameid = :id";
$stmt2 = $dbh->prepare($query2);
$stmt2->bindValue(':id', $id, PDO::PARAM_INT);
$stmt2->execute();

// Fetch all of the remaining rows in the result set //

$result = $stmt2->fetchAll();

foreach( $result as $row ) {

$i = $i + 1;
}

if ($i == 7) {

echo '.report_table {height: 27px;}';

} elseif ($i == 6) {

echo '.report_table {height: 45px;}';

}  elseif ($i == 5) {
 echo '.report_table {height: 50px;}';

}  elseif ($i == 4) {
 echo '.report_table {height: 65px;}';

}  elseif ($i == 3) {
 echo '.report_table {height: 85px;}';

} elseif ($i == 2) {
 echo '.report_table {height: 100px;}';

} elseif ($i == 1) {
 echo '.report_table {height: 180px;}';
}


?>

select {
    width: 200px;
    height: 40px;
    font-size: 20px;
    margin-bottom: 10px;
margin-top: 10px;
}

.site_wrapper {
width: 1024px !important;
}

textarea {
    width: 200px;
    height: 20px;
    font-size: 20px;
    float: left;

    margin-left: 30px;
}

#download img{

	margin-right: 15px;
}

a img {
	margin-right: 15px;
}

.clip_div a {
display: inline-block;
height: 50px;
	
}
.js-textareacopybtn:hover {
	height: 50px;
	margin: 0;
	padding: 0;
	font-size: 30px;
	background: none;
	border: none;
	color: blue;
	text-decoration: underline;
cursor: pointer;
}
.js-textareacopybtn {
	height: 100px;
	margin: 0;
	padding: 0;
	font-size: 30px;
	background: none;
	border: none;
	color: blue;
	text-decoration: underline;
	
}
.clip_div p {
display: inline-block;
margin: 0;
	
}
.clip_div {
    float: right;
    width: 50%;
    margin: 30px 0px 60px 0px;
}

.gameinfo p {
	margin-bottom: 10px;
	
}

.gameinfo {
	font-size: 20px;
	clesar:both;
	
}

.game_table {
	width: 100%;
	margin: 0 auto;
	border: none;
margin-bottom: 30px;

}
</style>
</head>
<div class="site_wrapper">

<?php


echo '<p class="viewas">View as</p>';

echo '<form action="game_report.php" method="get" enctype="application/x-www-form-urlencoded">';
echo '<input type="hidden" name="id" value="';
echo $id;
echo '">';
echo '<select name="user" onchange="this.form.submit()">';

// Select user with highest overall score from DB //

$query2 = "SELECT * FROM user_game_score WHERE gameid = :id ORDER BY overall_score DESC";
$stmt2 = $dbh->prepare($query2);
$stmt2->bindValue(':id', $id, PDO::PARAM_INT);
$stmt2->execute();

// Fetch all of the remaining rows in the result set //

$result = $stmt2->fetchAll();

foreach( $result as $row ) {

echo '<option value="';
echo $row['username'];
if ($row['username'] == $user_m) {
	echo '" selected';
} else {
echo '"';
}
echo '>';
echo $row['username'];
echo '</option>';



}

echo '</select>';
echo '</form>';

echo '<div id="background_png" class="hideme"></div>';
echo '<div id="background" style="width: 1024px; height: 768px; margin: 0 auto; background-image: url(\'img/';
echo outcomeImage($outcome_m, $race_m); 
echo "'); cursor: url('img/reg_cursor.png'), default;\">";

// GET ALL VARS FOR CSS BAR PERCENTS


// Select from DB //

$query2 = "SELECT * FROM user_game_score WHERE gameid = :id ORDER BY units_produced DESC LIMIT 1";
$stmt2 = $dbh->prepare($query2);
$stmt2->bindValue(':id', $id, PDO::PARAM_INT);
$stmt2->execute();

// Fetch all of the remaining rows in the result set //

$result = $stmt2->fetchAll();

foreach( $result as $row ) {

$max_units = $row['units_produced'];


}


// Select from DB //

$query2 = "SELECT * FROM user_game_score WHERE gameid = :id ORDER BY structures_constructed DESC LIMIT 1";
$stmt2 = $dbh->prepare($query2);
$stmt2->bindValue(':id', $id, PDO::PARAM_INT);
$stmt2->execute();

// Fetch all of the remaining rows in the result set //

$result = $stmt2->fetchAll();

foreach( $result as $row ) {

$max_buildings = $row['structures_constructed'];


}

// Select from DB //

$query2 = "SELECT * FROM user_game_score WHERE gameid = :id ORDER BY gold_mined DESC LIMIT 1";
$stmt2 = $dbh->prepare($query2);
$stmt2->bindValue(':id', $id, PDO::PARAM_INT);
$stmt2->execute();

// Fetch all of the remaining rows in the result set //

$result = $stmt2->fetchAll();

foreach( $result as $row ) {

$max_gold = $row['gold_mined'];


}

// Select from DB //

$query2 = "SELECT * FROM user_game_score WHERE gameid = :id ORDER BY lumber_harvested DESC LIMIT 1";
$stmt2 = $dbh->prepare($query2);
$stmt2->bindValue(':id', $id, PDO::PARAM_INT);
$stmt2->execute();

// Fetch all of the remaining rows in the result set //

$result = $stmt2->fetchAll();

foreach( $result as $row ) {

$max_lumber = $row['lumber_harvested'];


}

// Select from DB //

$query2 = "SELECT * FROM user_game_score WHERE gameid = :id ORDER BY oil_harvested DESC LIMIT 1";
$stmt2 = $dbh->prepare($query2);
$stmt2->bindValue(':id', $id, PDO::PARAM_INT);
$stmt2->execute();

// Fetch all of the remaining rows in the result set //

$result = $stmt2->fetchAll();

foreach( $result as $row ) {

$max_oil = $row['oil_harvested'];


}

// Select from DB //

$query2 = "SELECT * FROM user_game_score WHERE gameid = :id ORDER BY units_killed DESC LIMIT 1";
$stmt2 = $dbh->prepare($query2);
$stmt2->bindValue(':id', $id, PDO::PARAM_INT);
$stmt2->execute();

// Fetch all of the remaining rows in the result set //

$result = $stmt2->fetchAll();

foreach( $result as $row ) {

$max_kills = $row['units_killed'];


}

// Select from DB //

$query2 = "SELECT * FROM user_game_score WHERE gameid = :id ORDER BY structures_razed DESC LIMIT 1";
$stmt2 = $dbh->prepare($query2);
$stmt2->bindValue(':id', $id, PDO::PARAM_INT);
$stmt2->execute();

// Fetch all of the remaining rows in the result set //

$result = $stmt2->fetchAll();

foreach( $result as $row ) {

$max_razings = $row['structures_razed'];


}

echo '<div class="report_header">';
echo '<div class="title_header">';
echo 'Outcome';
echo '</div>';
echo '<div class="title_header">';
echo 'Rank';
echo '</div>';
echo '<div class="title_header">';
echo 'Score';
echo '</div>';

echo '<div class="outcome">';
echo outcomeText($outcome_m);
echo '</div>';
echo '<div class="rank">';
echo $rank_m;
echo '</div>';
echo '<div class="score">';
echo $overall_score_m;
echo '</div>';
echo '</div>';



// Select from DB //

$query3 = "SELECT * FROM user_game_score WHERE gameid = :id";
$stmt3 = $dbh->prepare($query3);
$stmt3->bindValue(':id', $id, PDO::PARAM_INT);
$stmt3->execute();

// Fetch all of the remaining rows in the result set //

$result = $stmt3->fetchAll();



echo '<table class="report_table">';
echo '<tbody>';
echo '<tr>';
echo '<th>Units</th>';
echo '<th>Buildings</th>';
echo '<th>Gold</th>';
echo '<th>Lumber</th>';
echo '<th>Oil</th>';
echo '<th>Kills</th>';
echo '<th>Razings</th>';
echo '</tr>';

$colors = array("red", "blue", "teal", "gray", "orange", "purple", "green", "brown");

$color_num = -1;

foreach( $result as $row ) {

$color_num = $color_num + 1;
echo '<table class="report_table">';
echo '<tbody>';
echo '<tr>';

echo '<td>';
?>
<div class="perc_100_box">

<span class="bignum_gr3"><span class="bignum_inner"><?php echo number_format($row['units_produced']); ?></span></span>
<div style="width: <?php echo returnPercent($row['units_produced'], $max_units); ?>%; background-color: <?php echo $colors[$color_num]; ?>; position: absolute;
z-index: 10; height: 100%; top: 0; left: 0; ">

</div>
</div>

<?php


echo '</td>';

echo '<td>';
?>
<div class="perc_100_box">

<span class="bignum_gr3"><span class="bignum_inner"><?php echo number_format($row['structures_constructed']); ?></span></span>
<div style="width: <?php echo returnPercent($row['structures_constructed'], $max_buildings); ?>%; background-color: <?php echo $colors[$color_num]; ?>; position: absolute;
z-index: 10; height: 100%; top: 0; left: 0; ">

</div>
</div>

<?php
echo '</td>';

echo '<td>';
?>
<div class="perc_100_box">

<span class="bignum_gr3"><span class="bignum_inner"><?php echo number_format($row['gold_mined']); ?></span></span>
<div style="width: <?php echo returnPercent($row['gold_mined'], $max_gold); ?>%; background-color: <?php echo $colors[$color_num]; ?>; position: absolute;
z-index: 10; height: 100%; top: 0; left: 0; ">

</div>
</div>

<?php
echo '</td>';

echo '<td>';
?>
<div class="perc_100_box">

<span class="bignum_gr3"><span class="bignum_inner"><?php echo number_format($row['lumber_harvested']); ?></span></span>
<div style="width: <?php echo returnPercent($row['lumber_harvested'], $max_lumber); ?>%; background-color: <?php echo $colors[$color_num]; ?>; position: absolute;
z-index: 10; height: 100%; top: 0; left: 0; ">

</div>
</div>

<?php
echo '</td>';

echo '<td>';
?>
<div class="perc_100_box">

<span class="bignum_gr3"><span class="bignum_inner"><?php echo number_format($row['oil_harvested']); ?></span></span>
<div style="width: <?php echo returnPercent($row['oil_harvested'], $max_oil); ?>%; background-color: <?php echo $colors[$color_num]; ?>; position: absolute;
z-index: 10; height: 100%; top: 0; left: 0; ">

</div>
</div>

<?php
echo '</td>';

echo '<td>';
?>
<div class="perc_100_box">

<span class="bignum_gr3"><span class="bignum_inner"><?php echo number_format($row['units_killed']); ?></span></span>
<div style="width: <?php echo returnPercent($row['units_killed'], $max_kills); ?>%; background-color: <?php echo $colors[$color_num]; ?>; position: absolute;
z-index: 10; height: 100%; top: 0; left: 0; ">

</div>
</div>

<?php
echo '</td>';

echo '<td>';
?>
<div class="perc_100_box">

<span class="bignum_gr3"><span class="bignum_inner"><?php echo number_format($row['structures_razed']); ?></span></span>
<div style="width: <?php echo returnPercent($row['structures_razed'], $max_razings); ?>%; background-color: <?php echo $colors[$color_num]; ?>; position: absolute;
z-index: 10; height: 100%; top: 0; left: 0; ">

</div>
</div>

<?php

echo '</tr>';


echo '</td>';
echo '</tbody>';
echo '</table>';

echo '<table class="report_table">';
echo '<tbody>';

echo '<tr>';
echo '<td class="center2">';
echo $row['username'];
echo ' (';
echo $row['race'];
echo ')';
echo ' - ';
echo $row['min_in_game'];
echo 'min - ';
echo substr(outcomeText($row['outcome']), 0, -1);
echo '</td>';


echo '</tr>';


echo '</tbody>';
echo '</table>';


}





?>

</tbody>
</table>
<span class="float_left link_to_gr">http://analytics.war2.co/game_report.php?id=<?php echo $id; ?><?php echo '&user='; echo $user_m; ?></span>
<span class="c_button" onclick="goBack()"><img src="img/continue_button.png"></span>
</div>
<div class="download_div">

<a href="#" id="download"><img src="img/dl.png">Download SS</a>
</div>
<div class="clip_div">

<p>
  <a href="#" class="js-textareacopybtn"><img src="img/copy.png">Copy Link</a>
</p>

<p>
  <textarea class="js-copytextarea">http://analytics.war2.co/game_report.php?id=<?php echo $id; ?>&user=<?php echo $user_m; ?></textarea>
</p>



</div>
<?php

// Select from DB //

$query = "SELECT * FROM game_info WHERE id = :id";
$stmt = $dbh->prepare($query);
$stmt->bindValue(':id', $id, PDO::PARAM_INT);
$stmt->execute();

// Fetch all of the remaining rows in the result set //

$result = $stmt->fetchAll();

// Display results from hack_history //
echo '<p class="gameinfo">Game Information</p>';

echo '<table class="game_table">';
echo '<tbody>';

if ($stmt->rowCount() > 0) {

foreach( $result as $row ) {

echo '<tr>';
echo '<td>Time</td>';
echo '<td class="utcdt">';
echo str_replace('-','/',$row['gametime']);
echo '</td>';
echo '</tr>';

echo '<tr>';
echo '<td>Name</td>';
echo '<td><a href="game_report_text.php?id=';
echo $row['id'];
echo '">';
echo $row['name'];
echo '</a></td>';
echo '</tr>';

echo '<tr>';
echo '<td>Map</td>';
echo '<td>';
echo $row['mapfile'];
echo '</td>';
echo '</tr>';

echo '<tr>';
echo '<td>Type</td>';
echo '<td>';
echo $row['gameoption'];
echo '</td>';
echo '</tr>';

echo '<tr>';
echo '<td>Players</td>';
echo '<td>';
echo $row['joins'];
echo '</td>';
echo '</tr>';
}

}


?>
<script src="html2canvas.js"></script>
<script src="canvas2image.js"></script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script>
function convertUTCDateToLocalDate(date) {
    var newDate = new Date(date.getTime()+date.getTimezoneOffset()*60*1000);
    var offset = date.getTimezoneOffset() / 60;
    var hours = date.getHours();
    newDate.setHours(hours - offset);
    return newDate;   
}


var x = document.getElementsByClassName("utcdt");
var i;
for (i = 0; i < x.length; i++) {
	var utcdt = x[i].innerHTML;
	var date = convertUTCDateToLocalDate(new Date(utcdt));
        x[i].innerHTML = date.toLocaleString();
    }




html2canvas([document.getElementById('background')], {
    onrendered: function(canvas) {
 canvas.setAttribute("id", "canvas");
     document.getElementById('background_png').appendChild(canvas);
 


    }
});


function downloadCanvas(link, canvasId, filename) {
    link.href = document.getElementById(canvasId).toDataURL();
    link.download = filename;
}

document.getElementById('download').addEventListener('click', function() {
    downloadCanvas(this, 'canvas', 'screenshot<?php echo $id ?>.png');
}, false);



function goBack() {
    window.history.back();
}

var copyTextareaBtn = document.querySelector('.js-textareacopybtn');

copyTextareaBtn.addEventListener('click', function(event) {
event.preventDefault();
  var copyTextarea = document.querySelector('.js-copytextarea');
  copyTextarea.select();

  try {
    var successful = document.execCommand('copy');
    var msg = successful ? 'successful' : 'unsuccessful';
    console.log('Copying text command was ' + msg);
  } catch (err) {
    console.log('Oops, unable to copy');
  }
});
</script>
</div>
</div>
</body>

</html>