
<?php

// Select leaderboard time online from user_stats DB //

$query81 = "SELECT * FROM game_info RIGHT JOIN user_game_score ON user_game_score.gameid = game_info.id where username = :search ORDER BY gametime DESC LIMIT 25";
$stmt81= $dbh->prepare($query81);
$stmt81->bindValue(':search', $user, PDO::PARAM_INT);

$stmt81->execute();



// Fetch all of the remaining rows in the result set //

$result = $stmt81->fetchAll();

// Display results from user_stats //

?>
<table>
<tbody>

<?php
$set_games = 'None';
if ($stmt81->rowcount() > 0){
$set_games = 'Yes';
?>
<div class="recent_games">
<div class="recent_games_inner">
<h2 class="center"><?php echo $user; ?> > Recent Games</h2>
<?php
echo '<tr>';
echo '<th>Name</th>';
echo '<th>Time</th>';
echo '<th>Map</th>';
echo '<th>Players</th>';
echo '</tr>';
echo '<tr>';



foreach( $result as $row ) {

	$name = $row["name"];
	$time = $row["gametime"];
	$map = $row["mapfile"];
	$id = $row["gameid"];
	$players = $row["joins"];
	


	echo '<td>';
	echo '<span><a href="game_report.php?id=';
	echo $id; 
	echo '&user=';
	echo $user;
	echo '">';
	echo $name;
	echo '</a>';
	echo "</span>";
	echo '</td>';

	echo '<td>';
	echo sprintf('<span class="utcdt">%s</span></span>', str_replace('-','/',$time));
	echo '</td>';

	echo '<td>';
	echo $map;
	echo '</td>';
	
	echo '<td>';
	echo $players;
	echo '</td>';

	echo '</tr>';

}

} else {

}

?>
</div>
</tbody>
</table>
</div>