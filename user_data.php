<?php



// Select user data from user_stats DB //

$query = "SELECT * FROM user_stats WHERE username LIKE :search";
$stmt = $dbh->prepare($query);
$stmt->bindValue(':search', $user, PDO::PARAM_INT);
$stmt->execute();

// Fetch all of the remaining rows in the result set //

$result = $stmt->fetchAll();
$row_count_user = $stmt->rowcount();

$set_user = 'Unknown';

// Display 'username' Not Found. If no user is returned //

if ($row_count_user < 1) {
	echo "<br>";
	echo "<br>";
	echo 'User ';
	echo "'";
	echo $user;
	echo "'";
	echo '<span class="notfound"> Not Found.</span>';
	echo "<br>";
	echo "<br>";
$set_user = 'None';
} else {
$set_user = 'Yes';
// Display results from user_stats //
foreach( $result as $row ) {
echo '<div class="user_profile_wrapper">';
echo '<div class="user_profile_left">';
$user = $row["username"];
	if ($online == 'yes') {

		echo '<h2> ';
		echo $row["username"];
		echo ' <img src="img/online2.png">';
		echo '</h2>';
	} else {

		echo '<h2>';
		echo $row["username"];
		echo ' <img src="img/offline2.png">';
		echo '</h2>';


	}

echo '<img src="img/';
echo $row["profile_img"];
echo '">';

$set_country = 'none';
if ($row["country"] == 'Unknown') { 
$set_country = 'Unknown';
} elseif ($row["country"] == '') {
$set_country = 'Unknown';
echo '<br>';
echo '<br>';
} elseif ($row["country"] == 'rom') {
echo '<br>';
echo '<br>';

} else { 
echo '<br>';
echo '<br>';
echo '<span>Country: ';
echo '<img src="';
echo $url;
echo 'icons/';
echo $row["country"];
echo '.gif">'; 
echo ' </span>';
echo strtoupper($row["country"]); 
echo '<br>';
}
$ah_status = $row["ah_status"];

if ($row["ah_status"] == 'AH') {
	echo '<span class="lg"><span>Anti-Hack: ';
	echo '<span class="green">';
	echo $row["ah_status"];
	echo '</span></span>';
} elseif ($row["ah_status"] == 'Unknown'){

} else {
	echo '<span class="pink"><span>Anti-Hack: ';
	echo '<span class="red">';
	echo $row["ah_status"];
	echo '</span></span>';
}


echo '</span>';
echo '<br>';
echo '<br>';

echo '<span>Total Logins: ';
echo number_format($row["total_logins"]);
echo '</span>';
echo '<br>';

echo '<span>Total Time Online: ';
$min = $row["total_online_min"];
$min = minToTime($min);
echo $min;
echo '</span>';
echo '<br>';

echo '<span>Average Time Online: ';
echo minToTime($row["avg_online"]);
echo '</span>';
echo '<br>';
echo '<br>';

}
echo '</div>';
echo '<div class="user_profile_sp">';
echo '<span><h3>Server Profile</h3> ';

if ($row["server_profile"]) {
	echo '<pre>';
	$stripped_prof = stripcslashes(htmlentities($row["server_profile"]));
	echo autolink($stripped_prof);
	echo $row["server_stats"];
	echo '</pre>';
	echo '</span>';
} else {
	echo "Unknown";
}

echo '</div>';




}
?>
