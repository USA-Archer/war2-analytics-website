<?php


// Select COUNT online accounts from tracker DB //

$query = "SELECT COUNT(*) FROM tracker WHERE logout IS NULL";
$stmt = $dbh->prepare($query);
$stmt->execute();

// Fetch all of the remaining rows in the result set //

$result = $stmt->fetchAll();

// Display results from user_stats //

foreach( $result as $row ) {
	$num_online = $row[0];


}

?>


<div class="online_now">
<div class="online_now_wrapper">
<h2>Currently Online (<?php echo $num_online; ?>)</h2>
<div class="online_results">

<?php

// Select online accounts from tracker DB //

$query = "SELECT * FROM tracker WHERE logout IS NULL ORDER BY id DESC";
$stmt = $dbh->prepare($query);
$stmt->execute();

// Fetch all of the remaining rows in the result set //

$result = $stmt->fetchAll();

// Display results from user_stats //

foreach( $result as $row ) {


	// Select online accounts from tracker DB //

	$query = "SELECT * FROM tracker WHERE logout IS NULL and username = :username";

	$stmt = $dbh->prepare($query);
	$stmt->bindValue(':username', $row["username"], PDO::PARAM_INT);
	$stmt->execute();

	// Fetch all of the remaining rows in the result set //

	$result = $stmt->fetchAll();

	// Display results from user_stats //

	foreach( $result as $row ) {
		date_default_timezone_set('UTC');

		$now = new DateTime();
		$login_time = $row["login"];
		$duration = $now->diff(new DateTime($login_time));
		$minutes = $duration->days * 24 * 60;
		$minutes += $duration->h * 60;
		$minutes += $duration->i;
		$duration_online_min = $minutes;

	}
	// Select new accounts from tracker DB //

	$query = "SELECT * FROM user_stats WHERE username = :username";
	$stmt = $dbh->prepare($query);
	$stmt->bindValue(':username', $row["username"], PDO::PARAM_INT);
	$stmt->execute();

	// Fetch all of the remaining rows in the result set //

	$result = $stmt->fetchAll();

	// Display results from user_stats //

	foreach( $result as $row ) {


		$username = $row["username"];

		echo '<span><a href="';
		echo $url;
		echo '/user.php?user=';
		echo $username; 
		echo '">';
		echo $username;
		echo '</a>';
		echo "</span>";
		// If AH is AH, display ah in green and country flag if set
		if ($row["ah_status"] == 'AH') {
			if ($row["country"] != '') {
				echo sprintf(' <img src="icons/%s.gif">', $row["country"]);
				echo ' <span>';
				echo strtoupper($row["country"]);
				echo '</span>';
			} else {
			}
			echo '<span> ';
			echo minToTime($duration_online_min);
			echo '</span>';

			echo '<span class="green"> ';
			echo $row["ah_status"];
			echo '</span>';
			echo '<br>';
			echo '<br>';

		// If AH is Uknown, there is no country to display or status			
		} elseif ($row["ah_status"] == 'Unknown') {
			echo '<span> ';
			echo minToTime($duration_online_min);
			echo '</span>';
			echo '<br>';
			echo '<br>';

		// Else AH is none so do red span class
		} else {
			if ($row["country"] != '') {
				echo sprintf(' <img src="icons/%s.gif">', $row["country"]);
				echo ' <span>';
				echo strtoupper($row["country"]);
				echo '</span>';
			} else {
				echo '<br>';
				echo '<br>';
		
			}
			echo '<span> ';
			echo minToTime($duration_online_min);
			echo ' </span>';	
			echo '<span class="red"> ';
			echo $row["ah_status"];
			echo '</span>';
			echo '<br>';
			echo '<br>';

	
		
		
	}
}
}

?>
</div>
</div>
</div>