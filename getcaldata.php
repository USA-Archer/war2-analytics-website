<?php



require 'db_config.php';


try {
$dbh = new PDO("mysql:dbname=$nameofdb;host=localhost", $dbusername, $dbpassword);
$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} 

catch (PDOException $e) {
echo 'Connection failed: ' . $e->getMessage();
}

$query = "SELECT MONTH(gametime), YEAR(gametime), COUNT(*) FROM game_info GROUP BY MONTH(gametime) ORDER BY YEAR(gametime) ASC";
$stmt = $dbh->prepare($query);
$stmt->execute();

// Fetch all of the remaining rows in the result set //

$result = $stmt->fetchAll();


$cal_array = array();

foreach( $result as $row ){

	$jd=gregoriantojd($row[0],03,1992);
	$month_name = jdmonthname($jd, 2);
	$month_name = "$month_name $row[1]";
	$cal_array = array(

		array("\'$month_name\', $row[2]")


		);
echo json_encode($cal_array);

}



?>