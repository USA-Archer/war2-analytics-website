
<div class="game_stats">
<div class="site_wrapper">
<div class="game_num center_big">

<div class="perc_100">
<span class="bignum_gr">

<?php echo $user_total_games; ?>
</span>

<h3>Games Played</h3>
</div>
</div>
</div>
</div>

<div class="game_stats">
<div class="site_wrapper">
<div class="game_num center_big">
<div class="perc_100">
<span class="bignum_gr">

<?php echo $game_time; ?>
</span>

<h3>Time in Game</h3>
</div>
</div>
</div>
</div>






<div class="site_wrapper">
<div class="user_score">


<div class="perc_100">
<span class="bignum_gr3"><?php echo $overall_score; ?></span>
<span class="score_caption">Overall Score</span>

</div>







<div class="perc_1429">
<span class="score_caption">Units</span>
<div class="perc_100_box">

<span class="bignum_gr3"><span class="bignum_inner"><?php echo $units; ?></span></span>
<div style="width: <?php echo returnPercent(str_replace(",", "", $units), $max_units); ?>%; background-color: blue; position: absolute;
z-index: 10; height: 100%; top: 0; left: 0; ">

</div>
</div>
</div>



<div class="perc_1429">
<span class="score_caption">Buildings</span>
<div class="perc_100_box">

<span class="bignum_gr3"><span class="bignum_inner"><?php echo $buildings; ?></span></span>
<div style="width: <?php echo returnPercent(str_replace(",", "", $buildings), $max_buildings); ?>%; background-color: blue; position: absolute;
z-index: 10; height: 100%; top: 0; left: 0; ">

</div>
</div>
</div>

<div class="perc_1429">
<span class="score_caption">Gold</span>
<div class="perc_100_box">

<span class="bignum_gr3"><span class="bignum_inner"><?php echo $gold; ?></span></span>
<div style="width: <?php echo returnPercent(str_replace(",", "", $gold), $max_gold); ?>%; background-color: blue; position: absolute;
z-index: 10; height: 100%; top: 0; left: 0; ">

</div>
</div>
</div>


<div class="perc_1429">
<span class="score_caption">Lumber</span>
<div class="perc_100_box">

<span class="bignum_gr3"><span class="bignum_inner"><?php echo $lumber; ?></span></span>
<div style="width: <?php echo returnPercent(str_replace(",", "", $lumber), $max_lumber); ?>%; background-color: blue; position: absolute;
z-index: 10; height: 100%; top: 0; left: 0; ">

</div>
</div>
</div>

<div class="perc_1429">
<span class="score_caption">Oil</span>
<div class="perc_100_box">

<span class="bignum_gr3"><span class="bignum_inner"><?php echo $oil; ?></span></span>
<div style="width: <?php echo returnPercent(str_replace(",", "", $oil), $max_oil); ?>%; background-color: blue; position: absolute;
z-index: 10; height: 100%; top: 0; left: 0; ">

</div>
</div>
</div>


<div class="perc_1429">
<span class="score_caption">Kills</span>
<div class="perc_100_box">

<span class="bignum_gr3"><span class="bignum_inner"><?php echo $kills; ?></span></span>
<div style="width: <?php echo returnPercent(str_replace(",", "", $kills), $max_kills); ?>%; background-color: blue; position: absolute;
z-index: 10; height: 100%; top: 0; left: 0; ">

</div>
</div>
</div>

<div class="perc_1429">
<span class="score_caption">Razings</span>
<div class="perc_100_box">

<span class="bignum_gr3"><span class="bignum_inner"><?php echo $razings; ?></span></span>
<div style="width: <?php echo returnPercent(str_replace(",", "", $razings), $max_razings); ?>%; background-color: blue; position: absolute;
z-index: 10; height: 100%; top: 0; left: 0; ">

</div>
</div>
</div>



</div>
</div>

