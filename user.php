<!DOCTYPE html >
<html>
<head>
<link rel="stylesheet" type="text/css" href="reset.css?ver=1">
<link rel="stylesheet" type="text/css" href="vanilla.css?ver=1">
<link rel="stylesheet" type="text/css" href="war2co.css?ver=97">
<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
<title>Warcraft 2 User Analytics | Analytics.War2.Co</title>
</head>
   
<body>
<h1><a href="http://war2.co">Analytics.War2.CO</a>: Warcraft 2 Analytics</h1>

<?php 

$s_page_active = 'no';
$u_page_active = 'yes';
$m_page_active = 'no';
$gs_page_active = 'no';
$l_page_active = 'no';


require 'nav.php';

?>

		<?php require 'user_header.php';?>

<div class="game_stats_img">
<div class="site_wrapper">
<div class="head_of_page"><h2 class="float_left"> War2.RU > User Analytics</h2></div>
</div>
<img src="img/war2.gif">
</div>
	<?php require 'functions.php';?>

<div class="user_form">
		<div class="site_wrapper">
			<form name="formSearch" method="get" action="user.php?user=" enctype="application/x-www-form-urlencoded">
				<input name="user" type="text" id="user_search" placeholder="Username...">  
				<input type="submit" value="Search" id="user_search_button">

			</form>
	
		</div>
	</div>
	<div class="site_wrapper">
		<?php require 'user_data.php'; ?>
		<?php require 'recent_logins.php'; ?>
		<?php require 'hack_history.php'; ?>
</div>
<?php 
require 'user_game_checker.php';
if ($set_games != 'None'){

require 'user_score.php';

} else {

} ?>
	<div class="site_wrapper">

<?php 
$s_page_active = 'yes';

if ($set_country !== 'Unknown' AND $set_country !== ' ' AND $set_user !== 'None'){

echo ' <div id="user_regions_div" style="width: 100%; height: 600px; margin-bottom: 3%"></div>';
} else {
}

?>


</div>
<?php 

if ($set_games != 'None'){

require 'user_recent_games.php';

echo '<div class="px_500_h">';
} else {
}

?>

<?php require 'user_map.php' ?>
</div>



</div>
</div>






		





	<div class="links">
		<div class="site_wrapper">
			<?php require 'links_from_profiles.php'; ?>
			<?php require 'new_accounts.php'; ?>
		</div>
	</div>
	
	
<div id="footer">
	<div class="site_wrapper">
		<h3><a href="http://war2.co/">War2.Co</a>: Warcraft 2 Analytics</h3>
	</div>
</div>




<?php require 'chartdata.php'; ?>


 <!--Load the AJAX API-->


<script type="text/javascript" src="http://www.google.com/jsapi"></script>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script type="text/javascript">
    

function getDateOfISOWeek(w, y) {
    var simple = new Date(y, 0, 1 + (w - 1) * 7);
    var dow = simple.getDay();
    var ISOweekStart = simple;
    if (dow <= 4)
        ISOweekStart.setDate(simple.getDate() - simple.getDay() + 1);
    else
        ISOweekStart.setDate(simple.getDate() + 8 - simple.getDay());
    return ISOweekStart;
}
    // Load the Visualization API and the piechart package.
    google.load('visualization', '1', {packages:['corechart', 'geochart', 'line', 'controls'], callback: drawChart});
    google.load('visualization', '1', {packages:['corechart', 'geochart', 'line', 'controls'], callback: drawRegionsMap});
      



// Geo chart begin





      	var jsonData = $.ajax({
          url: "user_getcountrydata.php?user=<?php echo $user; ?>",
          dataType: "json",
          async: false
          }).responseText;
console.log(jsonData.indexOf('"v":""'));
          
if (jsonData.indexOf('"v":""') == -1) {

      google.charts.setOnLoadCallback(drawRegionsMap);

      function drawRegionsMap() {
    


        var data = new google.visualization.DataTable(jsonData);

        var options = {legend: 'none'};

        var chart42 = new google.visualization.GeoChart(document.getElementById('user_regions_div'));

        chart42.draw(data, options);
}
} else {
}



            var products = <?php echo $pie_array_user ?>;

if (products.length > 1) {


    google.charts.setOnLoadCallback(drawChart);
      function drawChart() {

				// Top 50 games pie FOR USER

var data = new google.visualization.DataTable();
      data.addColumn('string', 'Map');
      data.addColumn('number', 'Count');





      data.addRows(products);

        var options = {
 backgroundColor:'transparent',
        };

        var chart9 = new google.visualization.PieChart(document.getElementById('piechart_user'));

        chart9.draw(data, options);
}
} else {

}
		

	 

	  

	  
    </script>
	<script>



	function convertUTCDateToLocalDate(utc_date) {
		var offset = new Date().getTimezoneOffset() * 60000;
		var newDate = utc_Date.setMinutes(utc_date.getMinutes() + offset);
		var newDate = new Date(newDate);
		return newDate;   
	}


	var x = document.getElementsByClassName("utcdt");
	var i;
	for (i = 0; i < x.length; i++) {
		var utcdt = x[i].innerHTML;
		var utcdt = utcdt + " UTC";
		var utcdt = new Date(utcdt);
		x[i].innerHTML = utcdt.toLocaleString();
		}


	</script>

	</body>
</html>
