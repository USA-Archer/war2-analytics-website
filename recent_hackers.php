<div class="recent_hackers">
<div class="rh_wrapper">
<h2>Recent <span>HACK!</span>ers</h2>
<div class="hacker_results">
<?php


// Select Recent hackers from  DB //

$query = "SELECT * FROM hack_history ORDER BY gametime DESC LIMIT 10";
$stmt = $dbh->prepare($query);
$stmt->execute();

// Fetch all of the remaining rows in the result set //

$result = $stmt->fetchAll();

// Display results of Recent Hackers //

if ($stmt->rowCount() > 0) {


foreach( $result as $row ) {  

echo sprintf('<span><span class="red">HACK!</span>er: <a href="user.php?user=%s">%s</a></span>', $row["username"], $row["username"]);
echo '<br>';
echo sprintf('<span>Time: <span class="utcdt">%s</span></span>', str_replace('-','/',$row["gametime"]));
echo '<br>';
echo sprintf('<span>Proof: <a href="proof.php?id=%s">Snapshot</a></span>', $row["id"]);
echo '<br>';
echo '<br>';


 }
}

?>
</div>
</div>
</div>