<?php
require 'db_config.php';



try {
$dbh = new PDO("mysql:dbname=$nameofdb;host=localhost", $dbusername, $dbpassword);
$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} 

catch (PDOException $e) {
echo 'Connection failed: ' . $e->getMessage();
}

// Get vars from GET //
if (isset($_GET['p1_user'], $_GET['p1_race'], $_GET['p1_outcome'], $_GET['map'], $_GET['players'], $_GET['from'], $_GET['to'])) {

$p1_user = str_replace(array('%'),'',$_GET['p1_user']); 
$p1_race = str_replace(array('%'),'',$_GET['p1_race']); 
$p1_outcome = str_replace(array('%'),'',$_GET['p1_outcome']); 

$p2_user = str_replace(array('%'),'',$_GET['p2_user']); 
$p2_race = str_replace(array('%'),'',$_GET['p2_race']); 
$p2_outcome = str_replace(array('%'),'',$_GET['p2_outcome']); 

$map = str_replace(array('%'),'',$_GET['map']); 
$players = str_replace(array('%'),'',$_GET['players']); 
$from = str_replace(array('%'),'',$_GET['from']);
$to = str_replace(array('%'),'',$_GET['to']);  







$query_start = "SELECT * FROM (Select * FROM user_game_score WHERE username = :p1_user";
$query_p1_race = " AND race = :p1_race";
$query_p1_outcome = " AND outcome = :p1_outcome";
$query_game_info = ") AS player1_score INNER JOIN ( SELECT * FROM game_info WHERE";
$query_map = " mapfile = :map";
$query_from = " AND gametime BETWEEN :from";
$query_to = " AND :to";
$query_players = " AND players = :players";

$query_p2_start = ") AS game_info ON player1_score.gameid = game_info.id INNER JOIN (SELECT * FROM user_game_score WHERE";
$query_p2_user = " username = :p2_user";
$query_p2_race = " AND race = :p2_race";
$query_p2_outcome = " AND outcome = :p2_outcome";
$query_p2_end = ") AS player2_score ON player2_score.gameid = game_info.id ORDER BY gametime DESC LIMIT 100";

$query_end = ") AS game_info ON player1_score.gameid = game_info.id ORDER BY gameid DESC LIMIT 100";

if ($p1_user != 'Any' && $p1_race == 'Any' && $p1_outcome == 'Any' && $map != 'Any' && $players == 'Any' ) {

$query = $query_start . $query_game_info . $query_map . $query_from . $query_to . $query_end;

$stmt = $dbh->prepare($query);
$stmt->bindValue(':p1_user', $p1_user, PDO::PARAM_STR);
$stmt->bindValue(':map', $map, PDO::PARAM_STR);
$stmt->bindValue(':from', $from, PDO::PARAM_STR);
$stmt->bindValue(':to', $to, PDO::PARAM_STR);


} elseif ($p1_user != 'Any' && $p1_race != 'Any' && $p1_outcome == 'Any' && $map != 'Any' && $players == 'Any' ) {

$query = $query_start . $query_p1_race . $query_game_info . $query_map . $query_from . $query_to . $query_end;


$stmt = $dbh->prepare($query);
$stmt->bindValue(':p1_user', $p1_user, PDO::PARAM_STR);
$stmt->bindValue(':p1_race', $p1_race, PDO::PARAM_STR);
$stmt->bindValue(':map', $map, PDO::PARAM_STR);
$stmt->bindValue(':from', $from, PDO::PARAM_STR);
$stmt->bindValue(':to', $to, PDO::PARAM_STR);




} elseif ($p1_user != 'Any' && $p1_race != 'Any' && $p1_outcome != 'Any' && $map != 'Any' && $players == 'Any' ) {


$query = $query_start . $query_p1_race . $query_p1_outcome . $query_game_info . $query_map . $query_from . $query_to . $query_end;


$stmt = $dbh->prepare($query);
$stmt->bindValue(':p1_user', $p1_user, PDO::PARAM_STR);
$stmt->bindValue(':p1_race', $p1_race, PDO::PARAM_STR);
$stmt->bindValue(':p1_outcome', $p1_outcome, PDO::PARAM_STR);
$stmt->bindValue(':map', $map, PDO::PARAM_STR);
$stmt->bindValue(':from', $from, PDO::PARAM_STR);
$stmt->bindValue(':to', $to, PDO::PARAM_STR);





} elseif ($p1_user != 'Any' && $p1_race != 'Any' && $p1_outcome != 'Any' && $map != 'Any' && $players != 'Any' ) {


$query = $query_start . $query_p1_race . $query_p1_outcome . $query_game_info . $query_map . $query_from . $query_to . $query_players . $query_end;


$stmt = $dbh->prepare($query);
$stmt->bindValue(':p1_user', $p1_user, PDO::PARAM_STR);
$stmt->bindValue(':p1_race', $p1_race, PDO::PARAM_STR);
$stmt->bindValue(':p1_outcome', $p1_outcome, PDO::PARAM_STR);
$stmt->bindValue(':map', $map, PDO::PARAM_STR);
$stmt->bindValue(':from', $from, PDO::PARAM_STR);
$stmt->bindValue(':to', $to, PDO::PARAM_STR);
$stmt->bindValue(':players', $players, PDO::PARAM_INT);




} elseif ( $p1_user != 'Any' && $p1_race == 'Any' && $p1_outcome != 'Any' && $map != 'Any' && $players == 'Any' ) {


$query = $query_start . $query_p1_outcome . $query_game_info . $query_map . $query_from . $query_to . $query_end;


$stmt = $dbh->prepare($query);
$stmt->bindValue(':p1_user', $p1_user, PDO::PARAM_STR);
$stmt->bindValue(':p1_outcome', $p1_outcome, PDO::PARAM_STR);
$stmt->bindValue(':map', $map, PDO::PARAM_STR);
$stmt->bindValue(':from', $from, PDO::PARAM_STR);
$stmt->bindValue(':to', $to, PDO::PARAM_STR);




} elseif ( $p1_user != 'Any' && $p1_race == 'Any' && $p1_outcome != 'Any' && $map != 'Any' && $players != 'Any' ) {

$query = $query_start . $query_p1_outcome . $query_game_info . $query_map . $query_from . $query_to . $query_players . $query_end;


$stmt = $dbh->prepare($query);
$stmt->bindValue(':p1_user', $p1_user, PDO::PARAM_STR);
$stmt->bindValue(':p1_outcome', $p1_outcome, PDO::PARAM_STR);
$stmt->bindValue(':map', $map, PDO::PARAM_STR);
$stmt->bindValue(':from', $from, PDO::PARAM_STR);
$stmt->bindValue(':to', $to, PDO::PARAM_STR);
$stmt->bindValue(':players', $players, PDO::PARAM_INT);


} elseif ( $p1_user != 'Any' && $p1_race == 'Any' && $p1_outcome == 'Any' && $map != 'Any' && $players != 'Any' ) {

$query = $query_start . $query_game_info . $query_map . $query_from . $query_to . $query_players . $query_end;


$stmt = $dbh->prepare($query);
$stmt->bindValue(':p1_user', $p1_user, PDO::PARAM_STR);
$stmt->bindValue(':map', $map, PDO::PARAM_STR);
$stmt->bindValue(':from', $from, PDO::PARAM_STR);
$stmt->bindValue(':to', $to, PDO::PARAM_STR);
$stmt->bindValue(':players', $players, PDO::PARAM_INT);

} elseif ( $p1_user != 'Any' && $p1_race == 'Any' && $p1_outcome == 'Any' && $map == 'Any' && $players == 'Any' ) {

$query_from = " gametime >= :from";

$query = $query_start . $query_game_info . $query_from . $query_to . $query_end;

$stmt = $dbh->prepare($query);
$stmt->bindValue(':p1_user', $p1_user, PDO::PARAM_STR);
$stmt->bindValue(':from', $from, PDO::PARAM_STR);
$stmt->bindValue(':to', $to, PDO::PARAM_STR);

} elseif ( $p1_user != 'Any' && $p1_race != 'Any' && $p1_outcome == 'Any' && $map == 'Any' && $players == 'Any' ) {

$query_from = " gametime >= :from";

$query = $query_start . $query_p1_race . $query_game_info . $query_from . $query_to . $query_end;


$stmt = $dbh->prepare($query);
$stmt->bindValue(':p1_user', $p1_user, PDO::PARAM_STR);
$stmt->bindValue(':p1_race', $p1_race, PDO::PARAM_STR);
$stmt->bindValue(':from', $from, PDO::PARAM_STR);
$stmt->bindValue(':to', $to, PDO::PARAM_STR);


} elseif ( $p1_user != 'Any' && $p1_race != 'Any' && $p1_outcome != 'Any' && $map == 'Any' && $players == 'Any' ) {


$query_from = " gametime >= :from";

$query = $query_start . $query_p1_race . $query_p1_outcome . $query_game_info . $query_from . $query_to . $query_end;


$stmt = $dbh->prepare($query);
$stmt->bindValue(':p1_user', $p1_user, PDO::PARAM_STR);
$stmt->bindValue(':p1_race', $p1_race, PDO::PARAM_STR);
$stmt->bindValue(':p1_outcome', $p1_outcome, PDO::PARAM_STR);

$stmt->bindValue(':from', $from, PDO::PARAM_STR);
$stmt->bindValue(':to', $to, PDO::PARAM_STR);



} elseif ( $p1_user != 'Any' && $p1_race != 'Any' && $p1_outcome != 'Any' && $map == 'Any' && $players != 'Any' ) {


$query_from = " gametime >= :from";

$query = $query_start . $query_p1_race . $query_p1_outcome . $query_game_info . $query_from . $query_to . $query_players . $query_end;


$stmt = $dbh->prepare($query);
$stmt->bindValue(':p1_user', $p1_user, PDO::PARAM_STR);
$stmt->bindValue(':p1_race', $p1_race, PDO::PARAM_STR);
$stmt->bindValue(':p1_outcome', $p1_outcome, PDO::PARAM_STR);
$stmt->bindValue(':from', $from, PDO::PARAM_STR);
$stmt->bindValue(':to', $to, PDO::PARAM_STR);
$stmt->bindValue(':players', $players, PDO::PARAM_INT);




} elseif ( $p1_user != 'Any' && $p1_race == 'Any' && $p1_outcome != 'Any' && $map == 'Any' && $players == 'Any' ) {

$query_from = " gametime >= :from";
$query = $query_start . $query_p1_outcome . $query_game_info . $query_from . $query_to . $query_end;


$stmt = $dbh->prepare($query);
$stmt->bindValue(':p1_user', $p1_user, PDO::PARAM_STR);
$stmt->bindValue(':p1_outcome', $p1_outcome, PDO::PARAM_STR);
$stmt->bindValue(':from', $from, PDO::PARAM_STR);
$stmt->bindValue(':to', $to, PDO::PARAM_STR);


} elseif ( $p1_user != 'Any' && $p1_race == 'Any' && $p1_outcome != 'Any' && $map == 'Any' && $players != 'Any' ) {

$query_from = " gametime >= :from";
$query = $query_start . $query_p1_outcome . $query_game_info . $query_from . $query_to . $query_players . $query_end;


$stmt = $dbh->prepare($query);
$stmt->bindValue(':p1_user', $p1_user, PDO::PARAM_STR);
$stmt->bindValue(':p1_outcome', $p1_outcome, PDO::PARAM_STR);
$stmt->bindValue(':map', $map, PDO::PARAM_STR);
$stmt->bindValue(':from', $from, PDO::PARAM_STR);
$stmt->bindValue(':to', $to, PDO::PARAM_STR);
$stmt->bindValue(':players', $players, PDO::PARAM_INT);




} elseif ( $p1_user != 'Any' && $p1_race == 'Any' && $p1_outcome == 'Any' && $map == 'Any' && $players != 'Any' ) {

$query_from = " gametime >= :from";
$query = $query_start . $query_game_info . $query_from . $query_to . $query_players . $query_end;


$stmt = $dbh->prepare($query);
$stmt->bindValue(':p1_user', $p1_user, PDO::PARAM_STR);
$stmt->bindValue(':from', $from, PDO::PARAM_STR);
$stmt->bindValue(':to', $to, PDO::PARAM_STR);
$stmt->bindValue(':players', $players, PDO::PARAM_INT);

} elseif ( $p1_user != 'Any' && $p1_race != 'Any' && $p1_outcome == 'Any' && $map != 'Any' && $players != 'Any' ) {

$query_from = " gametime >= :from";
$query = $query_start . $query_p1_race . $query_game_info . $query_map . ' AND' . $query_from . $query_to . $query_players . $query_end;
 

$stmt = $dbh->prepare($query);
$stmt->bindValue(':p1_user', $p1_user, PDO::PARAM_STR);
$stmt->bindValue(':map', $map, PDO::PARAM_STR);
$stmt->bindValue(':from', $from, PDO::PARAM_STR);
$stmt->bindValue(':to', $to, PDO::PARAM_STR);
$stmt->bindValue(':p1_race', $p1_race, PDO::PARAM_STR);
$stmt->bindValue(':players', $players, PDO::PARAM_INT);



} elseif ( $p1_user != 'Any' && $p1_race == 'Any' && $p1_outcome != 'Any' && $map != 'Any' && $players != 'Any' && $p2_user != 'Any' && $p2_race == 'Any' && $p2_outcome != 'Any') {

$query = $query_start . $query_p1_outcome . $query_game_info . $query_map . $query_from . $query_to . $query_players . $query_p2_start . $query_p2_user . $query_p2_outcome . $query_p2_end;

$stmt = $dbh->prepare($query);
$stmt->bindValue(':p1_user', $p1_user, PDO::PARAM_STR);
$stmt->bindValue(':p1_outcome', $p1_outcome, PDO::PARAM_STR);
$stmt->bindValue(':map', $map, PDO::PARAM_STR);
$stmt->bindValue(':from', $from, PDO::PARAM_STR);
$stmt->bindValue(':to', $to, PDO::PARAM_STR);
$stmt->bindValue(':p2_user', $p2_user, PDO::PARAM_STR);
$stmt->bindValue(':p2_outcome', $p2_outcome, PDO::PARAM_STR);
$stmt->bindValue(':players', $players, PDO::PARAM_INT);


} else {

}



$stmt->execute();

// Fetch all of the remaining rows in the result set //

$result = $stmt->fetchAll();


require 'search_report_results.php';



} else {

echo "Not Found.";

}

?>