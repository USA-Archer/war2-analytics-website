<div class="profile_links">
<h2>Links in Profiles</h2>
<div class="link_results">
<?php
// Select links from profiles in user_stats DB //

$query = "SELECT * FROM user_stats WHERE server_profile REGEXP 'http' OR 'https' ORDER BY id DESC";
$stmt = $dbh->prepare($query);
$stmt->execute();

// Fetch all of the remaining rows in the result set //

$result = $stmt->fetchAll();

// Display results from user_stats //


foreach( $result as $row ) {
	$username = $row["username"];
	$profile = $row["server_profile"];
	echo autolink2($profile, $username);



}


?>
</div>
</div>