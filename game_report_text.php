<!DOCTYPE html >

<head>
<meta name="viewport" content="initial-scale=1.0, user-scalable=yes" />
<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
<title>Warcraft 2 Analytics | War2.Co</title>
</head>


<?php
require 'db_config.php';


try {
$dbh = new PDO("mysql:dbname=$nameofdb;host=localhost", $dbusername, $dbpassword);
$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} 

catch (PDOException $e) {
echo 'Connection failed: ' . $e->getMessage();
}

// Get id from GET //

$id = str_replace(array('%'),'',$_GET['id']); 

// If id not present, Quit //

if (!$id)
{
    exit;
}

// Select from DB //

$query = "SELECT * FROM game_info WHERE id = :id";
$stmt = $dbh->prepare($query);
$stmt->bindValue(':id', $id, PDO::PARAM_INT);
$stmt->execute();

// Fetch all of the remaining rows in the result set //

$result = $stmt->fetchAll();

// Display results from hack_history //



if ($stmt->rowCount() > 0) {

foreach( $result as $row ) {
echo '<p><a href="';
echo $row['gr_link'];
echo '">';
echo $row['gr_link'];
echo '</a></p>';
echo '<pre>';
echo $row['gamereport'];
echo '</pre>';
}

}

?>



<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script>
function convertUTCDateToLocalDate(date) {
    var newDate = new Date(date.getTime()+date.getTimezoneOffset()*60*1000);
    var offset = date.getTimezoneOffset() / 60;
    var hours = date.getHours();
    newDate.setHours(hours - offset);
    return newDate;   
}


var x = document.getElementsByClassName("utcdt");
var i;
for (i = 0; i < x.length; i++) {
	var utcdt = x[i].innerHTML;
	var date = convertUTCDateToLocalDate(new Date(utcdt));
        x[i].innerHTML = date.toLocaleString();
    }

</script>
</body>

</html>