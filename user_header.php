
<?php
require 'db_config.php';

try {
$dbh = new PDO("mysql:dbname=$nameofdb;host=localhost", $dbusername, $dbpassword);
$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} 

catch (PDOException $e) {
echo 'Connection failed: ' . $e->getMessage();
}

// Get user from GET //

$user = str_replace(array('%'),'',$_GET['user']); 
$is_random = 'no';

// If user not present, get random user //

if (!$user)
{


// Select count of rows from user_stats DB //

$query = "SELECT COUNT(*) FROM user_stats";
$stmt = $dbh->prepare($query);
$stmt->execute();

// Fetch all of the remaining rows in the result set //

$result = $stmt->fetchAll();

// Display results from user_stats //
foreach( $result as $row ) {
$count_of_rows = $row[0];
}


$random_limit = rand(1,$count_of_rows);

// Select random user id from user_stats DB //

$query = "SELECT * FROM user_stats LIMIT :search2,1";
$stmt = $dbh->prepare($query);
$stmt->bindValue(':search2', $random_limit, PDO::PARAM_INT);


$stmt->execute();

// Fetch all of the remaining rows in the result set //

$result = $stmt->fetchAll();

// Display results from user_stats //
foreach( $result as $row ) {

$user = $row["username"];

}

$is_random = 'yes';

}
// User randomize done //

// Find out if user is online //

$query = "SELECT * FROM tracker WHERE username LIKE :search ORDER by id DESC LIMIT 1";
$stmt = $dbh->prepare($query);
$stmt->bindValue(':search', $user, PDO::PARAM_INT);
$stmt->execute();

// Fetch all of the remaining rows in the result set //

$result = $stmt->fetchAll();

// Display results from user_stats //
foreach( $result as $row ) {

if ($row["logout"] === NULL) {

	$online = "yes";

} else {

	$online = "no";

}
}

?>