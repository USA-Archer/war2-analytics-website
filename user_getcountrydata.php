<?php

$user = str_replace(array('%'),'',$_GET['user']); 

require 'db_config.php';


try {
$dbh = new PDO("mysql:dbname=$nameofdb;host=localhost", $dbusername, $dbpassword);
$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} 

catch (PDOException $e) {
echo 'Connection failed: ' . $e->getMessage();
}

$query = "SELECT country FROM user_stats where username = :search";
$stmt = $dbh->prepare($query);
$stmt->bindValue(':search', $user, PDO::PARAM_INT);
$stmt->execute();

// Fetch all of the remaining rows in the result set //

$result = $stmt->fetchAll();



  echo "{ \"cols\": [ {\"id\":\"\",\"label\":\"Country\",\"type\":\"string\"}, {\"id\":\"\",\"label\":\"Number\",\"type\":\"number\"}], \"rows\": [ ";
  $total_rows = $stmt->rowCount();
  $row_num = 0;


  foreach( $result as $row ){


$country_code = $row['country'];
$query21 = "SELECT name FROM country_stats where country = :search";
$stmt21 = $dbh->prepare($query21);
$stmt21->bindValue(':search', $country_code, PDO::PARAM_INT);
$stmt21->execute();

$result2 = $stmt21->fetchAll();

  foreach( $result2 as $row2 ){

$country_name = $row2[0];

}


    $row_num++;
    if ($row_num == $total_rows){

      echo "{\"c\":[{\"v\":\"";
      echo strtoupper($country_name);



      echo "\",\"f\":null},{\"v\":1,\"f\":null}]}";
    } else {
      echo "{\"c\":[{\"v\":\"";
      echo strtoupper($country_name);
      echo "\",\"f\":null},{\"v\":1,\"f\":null}]}, ";

     
    }
  }
  echo " ] }";

?>


