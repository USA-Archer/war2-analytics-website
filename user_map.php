

<?php
// Select leaderboard avg online from user_stats DB //


$query88 = "SELECT mapfile, COUNT(*) FROM game_info RIGHT JOIN user_game_score ON user_game_score.gameid = game_info.id where username = :search GROUP BY mapfile ORDER BY COUNT(*) DESC LIMIT 50";
$stmt88= $dbh->prepare($query88);
$stmt88->bindValue(':search', $user, PDO::PARAM_INT);

$stmt88->execute();


// Fetch all of the remaining rows in the result set //

$result = $stmt88->fetchAll();

// Display results from user_stats //


$i = 0;

if ($stmt81->rowcount() > 0){

?>
<div class="site_wrapper">
<h2 class="center"><?php echo $user; ?> > Top 50 Maps</h2>
<div class="leaderboard_map2">
<div class="map_table2">
<table>
<tbody>
<?php

foreach( $result as $row ) {
	$i = $i + 1;
	$map = $row[0];
	$count = $row[1];
	echo '<tr>';
	echo '<td>';
	echo addOrdinalNumberSuffix($i);
	echo '</td>';
	echo '<td>';
	echo '<span>';
	echo $map;
	echo "</span>";
	echo '</td>';
	echo '<td>';
	echo '<span> ';
	echo $count;
	echo '</span>';
	echo '</td>';
	echo '</tr>';

}
?>
</tbody>

</table>
</div>
</div>
<div class="pie_right2">
		<div id="piechart_user"></div>
<?php
} else {

}
?>


