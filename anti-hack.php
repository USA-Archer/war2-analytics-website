<?php

// Select user data from user_stats DB //

$query = "SELECT * FROM total_stats";
$stmt = $dbh->prepare($query);
$stmt->execute();

// Fetch all of the remaining rows in the result set //

$result = $stmt->fetchAll();

// Display results from total_stats //

if ($stmt->rowCount() > 0) {

foreach( $result as $row ) {
	$ah_hack = $row["ah_hack"];
	$ah_enabled = $row["ah_enabled"];
	$ah_none = $row["ah_none"];
}
}


$ah_total = $ah_none + $ah_hack + $ah_enabled;
$ah_percent = round(100 * (($ah_enabled + $ah_hack)/$ah_total));

?>
<div class="anti-hack">
<div class="anti-hack_wrapper">
<h2>Anti-Hack</h2>
<div style="text-align: center;">

<div id="donutchart" style="width: 100%; height: auto;"></div>
<div class="percent_back"><?php echo $ah_percent; ?>%</div></div>

<br>
<span><span class="green">AH</span>: <?php echo number_format($ah_enabled); ?> </span>
<span><span class="red">HACK!</span>: <?php echo number_format($ah_hack); ?> </span>
<span>none: <?php echo number_format($ah_none); ?></span>
<br>
</div>
</div>
