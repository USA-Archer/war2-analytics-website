<?php

require 'db_config.php';


try {
$dbh = new PDO("mysql:dbname=$nameofdb;host=localhost", $dbusername, $dbpassword);
$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} 

catch (PDOException $e) {
echo 'Connection failed: ' . $e->getMessage();
}



// BEGIN MONTH ARRAY GAMES


$query = "SELECT MONTH(gametime), YEAR(gametime), COUNT(*) FROM game_info GROUP BY MONTH(gametime), YEAR(gametime) ORDER BY YEAR(gametime), MONTH(gametime) ASC";
$stmt = $dbh->prepare($query);
$stmt->execute();

// Fetch all of the remaining rows in the result set //

$result = $stmt->fetchAll();


$months_games_array = array();
$row_num = 0;

foreach( $result as $row ){
	if ($row_num == 0){
		$row_num = $row_num + 1;
		continue;

	} else {
	

		$time_val = $row[1] . '/' . $row[0] . '/01 00:00:00 UTC';
		$months_games_array[] = array($time_val, intval($row[2]));
		$row_num = $row_num + 1;
		
	}

}

$months_games_array = json_encode($months_games_array);




// BEGIN MONTH ARRAY MINUTES


$query2 = "SELECT MONTH(gametime), YEAR(gametime), SUM(user_game_score.min_in_game) FROM game_info RIGHT JOIN user_game_score ON user_game_score.gameid = game_info.id GROUP BY MONTH(gametime), YEAR(gametime) ORDER BY YEAR(gametime), MONTH(gametime) ASC;";
$stmt2 = $dbh->prepare($query2);
$stmt2->execute();

// Fetch all of the remaining rows in the result set //

$result = $stmt2->fetchAll();


$row_num = 0;
$array_num = 0;

foreach( $result as $row ){
	if ($row_num == 0){
		$row_num = $row_num + 1;
		continue;

	} else {
		$row_num = $row_num + 1;
		$time_val = $row[1] . '/' . $row[0] . '/01 00:00:00 UTC';
		$months_minutes_array[] = array($time_val, intval($row[2]));

	}

}

$months_minutes_array = json_encode($months_minutes_array);




// BEGIN WEEK GAMES ARRAY


$query3 = "SELECT WEEK(gametime), YEAR(gametime), COUNT(*) FROM game_info GROUP BY YEAR(gametime), WEEK(gametime) ORDER BY YEAR(gametime), WEEK(gametime) ASC";
$stmt3 = $dbh->prepare($query3);
$stmt3->execute();

// Fetch all of the remaining rows in the result set //

$result = $stmt3->fetchAll();


$weeks_games_array = array();
$row_num = 0;



foreach( $result as $row ){
	if ($row_num == 0){
		$row_num = $row_num + 1;
		continue;

	} else {


		$weeks_games_array[] = array($row[0], $row[1], intval($row[2]));
		$row_num = $row_num + 1;
	}

}

$weeks_games_array = json_encode($weeks_games_array);



// BEGIN WEEKS MINUTES ARRAY



$query4 = "SELECT WEEK(gametime), YEAR(gametime), SUM(user_game_score.min_in_game) FROM game_info RIGHT JOIN user_game_score ON user_game_score.gameid = game_info.id GROUP BY WEEK(gametime), YEAR(gametime) ORDER BY YEAR(gametime), WEEK(gametime) ASC;";
$stmt4 = $dbh->prepare($query4);
$stmt4->execute();

// Fetch all of the remaining rows in the result set //

$result = $stmt4->fetchAll();


$weeks_minutes_array = array();
$row_num = 0;



foreach( $result as $row ){
	if ($row_num == 0){
		$row_num = $row_num + 1;
		continue;

	} else {


		$weeks_minutes_array[] = array($row[0], intval($row[1]), intval($row[2]));
		$row_num = $row_num + 1;
	}

}

$weeks_minutes_array = json_encode($weeks_minutes_array);



// HOURS GAMES AND MINUTES

// SUM OF GAMES AND MIN IN GAME
$query6 = "SELECT * FROM ( SELECT HOUR(gametime), DAY(gametime), MONTH(gametime), YEAR(gametime), id, COUNT(*), DATE_FORMAT(gametime, '%Y%m%d%H')  FROM game_info GROUP BY HOUR(gametime), DAY(gametime), MONTH(gametime), YEAR(gametime) ORDER BY DATE_FORMAT(gametime, '%Y%m%d%H') ASC ) AS table_a INNER JOIN ( SELECT gameid, HOUR(gametime), DAY(gametime), MONTH(gametime), YEAR(gametime), SUM(user_game_score.min_in_game), DATE_FORMAT(gametime, '%Y%m%d%H') FROM game_info INNER JOIN user_game_score ON user_game_score.gameid = game_info.id GROUP BY HOUR(gametime), DAY(gametime), MONTH(gametime), YEAR(gametime) ORDER BY DATE_FORMAT(gametime, '%Y%m%d%H') ASC ) AS table_b ON table_a.id = table_b.gameid";
$stmt6 = $dbh->prepare($query6);
$stmt6->execute();

// Fetch all of the remaining rows in the result set //

$result = $stmt6->fetchAll();


$hours_gm_array = array();
$row_num = 0;



foreach( $result as $row ){
	if ($row_num == 0){
		$row_num = $row_num + 1;
	
		continue;

	} else {

		$time_val = $row[3] . '/' . $row[2] . '/' . $row[1] . ' ' . $row[0] . ':00:00 UTC';

		$hours_gm_array[] = array($time_val, intval($row[5]), intval($row[12]));
		$row_num = $row_num + 1;
	}

}




$hours_gm_array = json_encode($hours_gm_array);


// PIE ARRAY TOP 50

$query7 = "SELECT mapfile, COUNT(*) from game_info GROUP BY mapfile ORDER by COUNT(*) DESC LIMIT 50";
$stmt7= $dbh->prepare($query7);
$stmt7->execute();

// Fetch all of the remaining rows in the result set //

$result = $stmt7->fetchAll();


$pie_array = array();



foreach( $result as $row ){


		$map = $row[0];
		$count = $row[1];
		$pie_array[] = array($map,intval($count));

	


}
$pie_array = json_encode($pie_array);


// PIE ARRAY TOP 50 USER

$query8 = "SELECT mapfile, COUNT(*) FROM game_info RIGHT JOIN user_game_score ON user_game_score.gameid = game_info.id where username = :search GROUP BY mapfile ORDER BY COUNT(*) DESC LIMIT 50";
$stmt8= $dbh->prepare($query8);
$stmt8->bindValue(':search', $user, PDO::PARAM_INT);

$stmt8->execute();

// Fetch all of the remaining rows in the result set //

$result = $stmt8->fetchAll();


$pie_array_user = array();

$row_count_user2 = $stmt8->rowcount();

if ($row_count_user2 < 1) {


} else {

foreach( $result as $row ){


		$map = $row[0];
		$count = $row[1];
		$pie_array_user[] = array($map,intval($count));

	

}
}
$pie_array_user = json_encode($pie_array_user);

?>
