<?php
if ($row_count_user > 0) {
echo '<div class="user_profile_rl">';
	echo '<h3>Recent Logins</h3>';
}


// Select user logins from tracker DB //
$query = "SELECT * FROM tracker WHERE username LIKE :search ORDER BY login DESC LIMIT 10";
$stmt = $dbh->prepare($query);
$stmt->bindValue(':search', $user, PDO::PARAM_INT);
$stmt->execute();

// Fetch all of the remaining rows in the result set //

$result = $stmt->fetchAll();

// Display results from tracker //

if ($stmt->rowcount() == 0 AND $row_count_user > 0) {
echo 'None.';
echo '<br>';
echo '<br>';
echo '</div>';

} else {

if ($row_count_user > 0) {
	echo '<div class="user_profile_rl_scroll">';	
}

foreach( $result as $row ) { 



	if ($row["logout"] != NULL) {
		echo '<span>Logout: ';
		echo '<span class="utcdt">';
		echo str_replace('-','/',$row["logout"]);
		echo '</span>';
		echo '</span>';
		echo '<br>';
		echo '<span>Login: ';
		echo '<span class="utcdt">';
		echo str_replace('-','/',$row["login"]);
		echo '</span>';
		echo '</span>';
		echo '<br>';
		echo '<span>Duration: ';
		echo minToTime($row["duration_in_min"]);
		echo '</span>';
		echo '<br>';
		echo '<br>';
	} else {
		date_default_timezone_set('UTC');

		$now = new DateTime();
		$login_time = $row["login"];
		$duration = $now->diff(new DateTime($login_time));
		$minutes = $duration->days * 24 * 60;
		$minutes += $duration->h * 60;
		$minutes += $duration->i;
		$duration_online_min = $minutes;

		echo '<span class="green lg">Online Now!</span>';
		echo '</span>';
		echo '<br>';
		echo '<span>Login: ';
		echo '<span class="utcdt">';
		echo str_replace('-','/',$row["login"]);
		echo '</span>';
		echo '</span>';
		echo '<br>';
		echo '<span>Duration: ';
		echo minToTime($duration_online_min);
		echo '</span>';
		echo '<br>';
		echo '<br>';
}


}

if ($row_count_user > 0) {
	echo '</div>';	
	echo '</div>';	
}

}
?>