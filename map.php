<!DOCTYPE html >
<html>
<head>
<link rel="stylesheet" type="text/css" href="reset.css?ver=1">
<link rel="stylesheet" type="text/css" href="vanilla.css?ver=1">
<link rel="stylesheet" type="text/css" href="war2co.css?ver=87">
<link rel="apple-touch-icon" sizes="57x57" href="/ico/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="/ico/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="/ico/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="/ico/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="/ico/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="/ico/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="/ico/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="/ico/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="/ico/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="/ico/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="/ico/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="/ico/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="/ico/favicon-16x16.png">
<link rel="manifest" href="/ico/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="/ico/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
<title>Warcraft 2 Analytics | War2.Co</title>
</head>
   
<body>
<h1><a href="http://war2.co">War2.Co</a>: Warcraft 2 Analytics</h1>

<?php 

$s_page_active = 'no';
$u_page_active = 'no';
$m_page_active = 'yes';
$gs_page_active = 'no';
$l_page_active = 'no';


require 'nav.php' ?>

		<?php require 'header.php';?>

<div class="game_stats_img">
<div class="site_wrapper">
<div class="head_of_page"><h2 class="float_left"> War2.RU > Map Analytics</h2></div>
</div>
<img src="img/war2.gif">
</div>
	<?php require 'functions.php';?>


	<div class="site_wrapper">
		<?php require 'user_data.php'; ?>
		<?php require 'recent_logins.php'; ?>
		<?php require 'hack_history.php'; ?>
</div>

<?php require 'user_score.php' ?>
	<div class="site_wrapper">

<?php 
$s_page_active = 'yes';

if ($set_country != 'Unknown'){

echo ' <div id="user_regions_div" style="width: 100%; height: 600px; margin-bottom: 3%"></div>';
} else {
}

?>


</div>
<?php require 'user_recent_games.php' ?>

<?php require 'user_map.php' ?>



</div>
</div>


	<div class="user_form">
		<div class="site_wrapper">
			<form name="formSearch" method="get" action="index.php?user=" enctype="application/x-www-form-urlencoded">
				<input name="user" type="text" id="user_search" placeholder="Username...">  
				<input type="submit" value="Search" id="user_search_button">

			</form>
	
		</div>
	</div>
	<div class="game_stats">
		<div class="site_wrapper">

	

			<?php require 'game_stats.php'; ?>
		</div>
	</div>
	<div class="site_wrapper">

    <div id="regions_div" style="width: 100%; height: 600px; margin-bottom: 3%"></div>


</div>


<div class="stats_row">
	<div class="site_wrapper">

		<?php require 'config.php' ?>
		<?php require 'community.php'; ?>
		<?php require 'recent_hackers.php'; ?>
		<?php require 'anti-hack.php'; ?>
		<?php require 'online_now.php'; ?>
</div>

</div>
<h2 class="center">All: Recent Games</h2>

<div class="recent_games">

<?php require 'recent_games.php'; ?>



	</div>
	<div class="site_wrapper">
<h2 class="center">All: Top 50 Maps</h2>
<div class="pie_right">
<div id="piechart" style="width: 100%; height: 500px;"></div>
</div>


<?php require 'leaderboard_map.php' ?>

</div>

		<div class="site_wrapper">
						<div class="leaderboards">
			<div class="leaderboards_wrapper">
				<h2>Leaderboards</h2>
				<?php require 'leaderboard_time_online.php'; ?>
				<?php require 'leaderboard_logins.php'; ?>
				<?php require 'leaderboard_avg_online.php'; ?>
			</div>
		</div>
</div>

	<div class="server_stats">
		<div class="site_wrapper">
			<?php require 'server_stats.php'; ?>
		</div>
	</div>



<div class="cal_div">
<div class="site_wrapper">
<h1>Server Stats by Hour</h1>
</div>
<div id="dashboard_div">
<div id="linechart_material_hour1" style="width: 100%; height: 500px;"></div>
<div id="range_filter_div" style="width: 100%"></div>
</div>

<div class="site_wrapper">
<h1>Server Stats by Week</h1>
</div>
<div id="linechart_material_week1" style="width: 100%; height: 500px;"></div>



<div class="site_wrapper">
<h1>Server Stats by Month</h1>
</div>
<div id="linechart_material" style="width: 100%; height: 500px;"></div>
	

	<div class="links">
		<div class="site_wrapper">
			<?php require 'links_from_profiles.php'; ?>
			<?php require 'new_accounts.php'; ?>
		</div>
	</div>
	
	
<div id="footer">
	<div class="site_wrapper">
		<h3><a href="http://war2.co/">War2.Co</a>: Warcraft 2 Analytics</h3>
	</div>
</div>




<?php require 'chartdata.php'; ?>


 <!--Load the AJAX API-->

<script type="text/javascript" src="http://www.google.com/jsapi"></script>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script type="text/javascript">
    

function getDateOfISOWeek(w, y) {
    var simple = new Date(y, 0, 1 + (w - 1) * 7);
    var dow = simple.getDay();
    var ISOweekStart = simple;
    if (dow <= 4)
        ISOweekStart.setDate(simple.getDate() - simple.getDay() + 1);
    else
        ISOweekStart.setDate(simple.getDate() + 8 - simple.getDay());
    return ISOweekStart;
}
    // Load the Visualization API and the piechart package.
    google.load('visualization', '1', {packages:['corechart', 'geochart', 'line', 'controls'], callback: drawChart});
    google.load('visualization', '1', {packages:['corechart', 'geochart', 'line', 'controls'], callback: drawRegionsMap});
      


// Geo chart begin


      google.charts.setOnLoadCallback(drawRegionsMap);

      function drawRegionsMap() {
      	var jsonData = $.ajax({
          url: "getcountrydata.php",
          dataType: "json",
          async: false
          }).responseText;
          

        var data = new google.visualization.DataTable(jsonData);

        var options = {};

        var chart4 = new google.visualization.GeoChart(document.getElementById('regions_div'));

        chart4.draw(data, options);




      	var jsonData = $.ajax({
          url: "user_getcountrydata.php?user=<?php echo $user; ?>",
          dataType: "json",
          async: false
          }).responseText;
console.log (Object.keys(jsonData).length);
          
if (Object.keys(jsonData).length != 167) {

        var data = new google.visualization.DataTable(jsonData);

        var options = {legend: 'none'};

        var chart42 = new google.visualization.GeoChart(document.getElementById('user_regions_div'));

        chart42.draw(data, options);
} else {
}



      }


// AH pie chart begin

      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {
	var jsonData = $.ajax({
          url: "getahdata.php",
          dataType: "json",
          async: false
          }).responseText;
          

        var data = new google.visualization.DataTable(jsonData);


        var options = {
          pieHole: .8,
 	  colors: ['green','red','#4CAF50'],
	  legend: 'none',
          pieSliceText: 'none',
	  backgroundColor: 'transparent'
        };

        var chart = new google.visualization.PieChart(document.getElementById('donutchart'));
        chart.draw(data, options);

				// Top 50 games pie FOR USER

var data = new google.visualization.DataTable();
      data.addColumn('string', 'Map');
      data.addColumn('number', 'Count');




      var products = <?php echo $pie_array_user ?>;

if (products.length > 1) {
      data.addRows(products);

        var options = {
 backgroundColor:'transparent',
        };

        var chart9 = new google.visualization.PieChart(document.getElementById('piechart_user'));

        chart9.draw(data, options);
} else {

}
		
		// Top 50 games pie

var data = new google.visualization.DataTable();
      data.addColumn('string', 'Map');
      data.addColumn('number', 'Count');




      var products = <?php echo $pie_array ?>;
      data.addRows(products);

        var options = {
 backgroundColor:'transparent',
        };


        var chart9 = new google.visualization.PieChart(document.getElementById('piechart'));

        chart9.draw(data, options);


	  
	  
// Months Games and Minutes Charts
	  
	  

	  
	  

      var data = new google.visualization.DataTable();
      data.addColumn('datetime', 'Weeks');
      data.addColumn('number', 'Games');
      data.addColumn('number', 'Minutes Played');

 

      var products = <?php echo $months_minutes_array ?>;




      var products2 = <?php echo $months_games_array ?>;
for (var i in products2) {

	var date_conv = new Date(products2[i][0]);
	products2[i][0] = date_conv;
	products2[i].splice(1,0);
	products2[i].push(products[i][1]);





}
      data.addRows(products2);

     var options = {


           seriesType: 'bars',    
 
            series: {

              1: {side: 'right', type: "line", targetAxisIndex:1}, // Right y-axis.
              2: {targetAxisIndex:0},
            },
            
              axes: {
            y: {
      				
              1: {side: 'right'} // Right y-axis.
            }
          },

          colors: ["lightblue", "green"],};      var chart6 = new google.visualization.ComboChart(document.getElementById('linechart_material'));

      chart6.draw(data, options);
	  
	  
	  
	  
// Weeks games and minutes array

      var data = new google.visualization.DataTable();
      data.addColumn('datetime', 'Weeks');
      data.addColumn('number', 'Games');
      data.addColumn('number', 'Minutes Played');

 





      var products = <?php echo $weeks_minutes_array ?>;
for (var i in products) {

	products[i][0] = "none";
	products[i].splice(1,1);




}



      var products2 = <?php echo $weeks_games_array ?>;
for (var i in products2) {

	var date_conv = getDateOfISOWeek(products2[i][0],products2[i][1]);
	products2[i][0] = new Date(date_conv);
	products2[i].splice(1,1);
	var date_conv = new Date(products2[i][0]);
	products2[i].push(products[i][1]);





}
      data.addRows(products2);

     var options = {


           seriesType: 'bars',    
 
            series: {

              1: {side: 'right', type: "line", targetAxisIndex:1}, // Right y-axis.
              2: {targetAxisIndex:0},
            },
            
              axes: {
            y: {
      				
              1: {side: 'right'} // Right y-axis.
            }
          },

          colors: ["lightblue", "green"],};      
		  
		  var chart5 = new google.visualization.ComboChart(document.getElementById('linechart_material_week1'));

      chart5.draw(data, options);

 // Hour Games and Minutes
 
  var data = new google.visualization.DataTable();

	data.addColumn('datetime', 'Weeks');
    data.addColumn('number', 'Games');
    data.addColumn('number', 'Minutes Played');

      var products2 = <?php echo $hours_gm_array ?>;


	for (var i in products2) {

		var date_conv = new Date(products2[i][0]);
		products2[i][0] = date_conv;



	}
		  data.addRows(products2);
    

    var rangeFilter = new google.visualization.ControlWrapper({
        controlType: 'ChartRangeFilter',
        containerId: 'range_filter_div',
        options: {
            filterColumnIndex: 0,
            ui: {
                chartOptions: {
                		colors: ["lightblue", "green"],
             seriesType: 'bars',    
 
            series: {

              1: {side: 'right', type: "line", targetAxisIndex:1}, // Right y-axis.
              2: {type: "bar", targetAxisIndex:0},
            },
            
              axes: {
            y: {
      				
              1: {side: 'right'} // Right y-axis.
            }
          },
                    height: 50,
                    width: '100%',
                    chartArea: {
                        width: '75%'
                    }
                },
                minRangeSize: 86400000, // 86400000ms = 1 day
                snapToData: true
            }
        },
        view: {
            columns: [0, 1, 2]
        },
        state: {
            range: {
                // set the starting range to
                start: new Date(new Date().getTime() - 60 * 60 * 24 * 1000 * 7),
                end: new Date()
            }
        }
    });
    
    var chart = new google.visualization.ChartWrapper({
        chartType: 'ComboChart',
        containerId: 'linechart_material_hour1',
        options: {
            // width and chartArea.width should be the same for the filter and chart
            height: 500,
            width: '100%',
            chartArea: {
                width: '75%'
            },


           seriesType: 'bars',    
 
            series: {

              1: {side: 'right', type: "line", targetAxisIndex:1}, // Right y-axis.
              2: {type: "bar", targetAxisIndex:0},
            },
            
              axes: {
            y: {
      				
              1: {side: 'right'} // Right y-axis.
            }
          },

          colors: ["lightblue", "green"]
    }});
    
    // Create the dashboard
    var dash = new google.visualization.Dashboard(document.getElementById('dashboard'));
    // bind the chart to the filter
    dash.bind([rangeFilter], [chart]);
    // draw the dashboard
    dash.draw(data);


	  
	  
	 

	  }

	  
    </script>
	<script>



	function convertUTCDateToLocalDate(utc_date) {
		var offset = new Date().getTimezoneOffset() * 60000;
		var newDate = utc_Date.setMinutes(utc_date.getMinutes() + offset);
		var newDate = new Date(newDate);
		return newDate;   
	}


	var x = document.getElementsByClassName("utcdt");
	var i;
	for (i = 0; i < x.length; i++) {
		var utcdt = x[i].innerHTML;
		var utcdt = utcdt + " UTC";
		var utcdt = new Date(utcdt);
		x[i].innerHTML = utcdt.toLocaleString();
		}


	</script>

	</body>
</html>
