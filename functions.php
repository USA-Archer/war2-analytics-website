<?php

function outcomeText($outcome) {

if ($outcome == 'WIN') {

$outcomeText = 'Victory!';

} elseif ($outcome == 'LOSS') {

$outcomeText = 'Defeat!';

} elseif ($outcome == 'DRAW') {

$outcomeText = 'DRAW!';

} elseif ($outcome == 'DISC') {

$outcomeText = 'Disconnect!';

}

return $outcomeText;
}
function cursorImage($race) {

if ($race == 'Human') { 

$cursor = "human_cursor.png"; 

} elseif ($race == 'Orc') { 


$cursor = "orc_cursor.gif"; 

} else {

$cursor = "reg_cursor.png";

}
return $cursor;
}


function outcomeImage($outcome, $race) {

if ($race == 'Human' && $outcome == 'WIN') {

$outcomeImage = 'hvictory1024.png';

} elseif ($race == 'Human' && $outcome == 'LOSS') {

$outcomeImage = 'hdefeat1024.png';

} elseif ($race == 'Human' && $outcome == 'DISC') {

$outcomeImage = 'hvictory1024.png';

} elseif ($race == 'Human' && $outcome == 'DRAW') {

$outcomeImage = 'hvictory1024.png';

} elseif ($race == 'Orc' && $outcome == 'WIN' || $outcome == 'DRAW' || $outcome == 'DISC') {

$outcomeImage = 'ovictory1024.png';

} elseif ($race == 'Orc' && $outcome == 'LOSS') {

$outcomeImage = 'odefeat1024.png';

}

return $outcomeImage;

}
function war2Rank($race, $score) {

// Human Ranks

if ($race == 'Human' && $score > 0 && $score <= 2000) {

$rank = 'Servant';

} elseif ($race == 'Human' && $score > 2000 && $score <= 5000) {

$rank = 'Peasant';

} elseif ($race == 'Human' && $score > 5000 && $score <= 8000) {

$rank = 'Squire';

} elseif ($race == 'Human' && $score > 8000 && $score <= 18000) {

$rank = 'Footman';

} elseif ($race == 'Human' && $score > 18000 && $score <= 28000) {

$rank = 'Corporal';

} elseif ($race == 'Human' && $score > 28000 && $score <= 40000) {

$rank = 'Sergeant';

} elseif ($race == 'Human' && $score > 40000 && $score <= 55000) {

$rank = 'Lieutenant';

} elseif ($race == 'Human' && $score > 55000 && $score <= 70000) {

$rank = 'Captain';

} elseif ($race == 'Human' && $score > 70000 && $score <= 85000) {

$rank = 'Major';

} elseif ($race == 'Human' && $score > 85000 && $score <= 105000) {

$rank = 'Knight';

} elseif ($race == 'Human' && $score > 105000 && $score <= 125000) {

$rank = 'General';

} elseif ($race == 'Human' && $score > 125000 && $score <= 145000) {

$rank = 'Admiral';

} elseif ($race == 'Human' && $score > 145000 && $score <= 165000) {

$rank = 'Marshall';

} elseif ($race == 'Human' && $score > 165000 && $score <= 185000) {

$rank = 'Lord';

} elseif ($race == 'Human' && $score > 185000 && $score <= 205000) {

$rank = 'Grand Admiral';

} elseif ($race == 'Human' && $score > 205000 && $score <= 230000) {

$rank = 'Highlord';

} elseif ($race == 'Human' && $score > 230000 && $score <= 255000) {

$rank = 'Thundergod';

} elseif ($race == 'Human' && $score > 255000 && $score <= 280000) {

$rank = 'God';

} elseif ($race == 'Human' && $score > 280000) {

$rank = '???';

}


// Orc Ranks

if ($race == 'Orc' && $score > 0 && $score <= 2000) {

$rank = 'Slave';

} elseif ($race == 'Orc' && $score > 2000 && $score <= 5000) {

$rank = 'Peon';

} elseif ($race == 'Orc' && $score > 5000 && $score <= 8000) {

$rank = 'Rogue';

} elseif ($race == 'Orc' && $score > 8000 && $score <= 18000) {

$rank = 'Grunt';

} elseif ($race == 'Orc' && $score > 18000 && $score <= 28000) {

$rank = 'Slasher';

} elseif ($race == 'Orc' && $score > 28000 && $score <= 40000) {

$rank = 'Marauder';

} elseif ($race == 'Orc' && $score > 40000 && $score <= 55000) {

$rank = 'Commander';

} elseif ($race == 'Orc' && $score > 55000 && $score <= 70000) {

$rank = 'Captain';

} elseif ($race == 'Orc' && $score > 70000 && $score <= 85000) {

$rank = 'Major';

} elseif ($race == 'Orc' && $score > 85000 && $score <= 105000) {

$rank = 'Ogre';

} elseif ($race == 'Orc' && $score > 105000 && $score <= 125000) {

$rank = 'General';

} elseif ($race == 'Orc' && $score > 125000 && $score <= 145000) {

$rank = 'Master';

} elseif ($race == 'Orc' && $score > 145000 && $score <= 165000) {

$rank = 'Marshall';

} elseif ($race == 'Orc' && $score > 165000 && $score <= 185000) {

$rank = 'Chieftain';

} elseif ($race == 'Orc' && $score > 185000 && $score <= 205000) {

$rank = 'Overlord';

} elseif ($race == 'Orc' && $score > 205000 && $score <= 230000) {

$rank = 'War Chief';

} elseif ($race == 'Orc' && $score > 230000 && $score <= 255000) {

$rank = 'Demigod';

} elseif ($race == 'Orc' && $score > 255000 && $score <= 280000) {

$rank = 'God';

} elseif ($race == 'Orc' && $score > 280000) {

$rank = '???';

}

return $rank;


}



function returnPercent($value,$max){

// Testing numbers. Replace with your own.

$scale = 1.0;

// Get Percentage out of 100
if ( !empty($max) ) { $percent = ($value * 100) / $max; } 
else { $percent = 0; }

// Limit to 100 percent (if more than the max is allowed)
if ( $percent > 100 ) { $percent = 100; }
return $percent;
}



function JSdate($in,$type){
    if($type=='date'){
        //Dates are patterned 'yyyy-MM-dd'
        preg_match('/(\d{4})-(\d{2})-(\d{2})/', $in, $match);
    } elseif($type=='datetime'){
        //Datetimes are patterned 'yyyy-MM-dd hh:mm:ss'
        preg_match('/(\d{4})-(\d{2})-(\d{2})\s(\d{2}):(\d{2}):(\d{2})/', $in, $match);
    }
     
    $year = (int) $match[1];
    $month = (int) $match[2] - 1; // Month conversion between indexes
    $day = (int) $match[3];
     
    if ($type=='date'){
        return "Date($year, $month, $day)";
    } elseif ($type=='datetime'){
        $hours = (int) $match[4];
        $minutes = (int) $match[5];
        $seconds = (int) $match[6];
        return "Date($year, $month, $day, $hours, $minutes, $seconds)";    
    }
}




function addOrdinalNumberSuffix($num) {
    if (!in_array(($num % 100),array(11,12,13))){
      switch ($num % 10) {
        // Handle 1st, 2nd, 3rd
        case 1:  return $num.'st';
        case 2:  return $num.'nd';
        case 3:  return $num.'rd';
      }
    }
    return $num.'th';
  }



function minToTime($mins) {

	if ($mins == 0) {
		return "0min";
	} else {
  		$m = floor($mins % 60);
  		$h = floor(($mins % 1440) / 60);
		$d = floor(($mins % 525600) / 1440);
		$y = floor($mins / 525600);


 		$m_word = ($m == 1 ? 'min' : 'min');
 		$h_word = ($h == 1 ? 'hr'   : 'hr');
 		$d_word = ($d == 1 ? 'd'    : 'd');
 		$y_word = ($d == 1 ? 'yr'    : 'yr');
		
		if (($d != 0) and ($h != 0) and ($m !=0) and ($y == 0)) {
 			return "$d$d_word $h$h_word $m$m_word";
		
		} elseif (($d == 0) and ($h != 0) and ($m !=0) and ($y == 0)) {
			return "$h$h_word $m$m_word";

		} elseif (($d == 0) and ($h == 0) and ($m !=0) and ($y == 0)) {
			return "$m$m_word";



		} elseif (($d != 0) and ($h == 0) and ($m !=0) and ($y == 0)) {
			return "$d$d_word $m$m_word";

		} elseif (($d != 0) and ($h != 0) and ($m == 0) and ($y == 0)) {
			return "$d$d_word $h$h_word";

		
		} elseif (($d == 0) and ($h != 0) and ($m == 0) and ($y == 0)) {
			return "$h$h_word";

		
		} elseif (($d != 0) and ($h != 0) and ($m !=0) and ($y != 0)) {
 			return "$y$y_word $d$d_word $h$h_word $m$m_word";

		} elseif (($d == 0) and ($h == 0) and ($m == 0) and ($y != 0)) {
			return "$y$y_Word";

		} elseif (($d != 0) and ($h == 0) and ($m == 0) and ($y != 0)) {
			return "$y$y_Word $d$d_word";
		
		} elseif (($d == 0) and ($h != 0) and ($m !=0) and ($y != 0)) {
			return "$y$y_Word $h$h_word $m$m_word";

		} elseif (($d == 0) and ($h == 0) and ($m !=0) and ($y != 0)) {
			return "$y$y_Word $m$m_word";


		} elseif (($d != 0) and ($h == 0) and ($m !=0) and ($y != 0)) {
			return "$y $y_word $d $d_word $m $m_word";

		} elseif (($d != 0) and ($h != 0) and ($m == 0) and ($y != 0)) {
			return "$y$y_Word $d$d_word $h$h_word";

		
		} elseif (($d == 0) and ($h != 0) and ($m == 0) and ($y != 0)) {
			return "$y$y_Word $h$h_word";
		} else {
	 		return "$y$y_word $d$d_word $h$h_word $m$m_word";
		}
 	 }	

}

function autolink($content){
    $re = '/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/';    
    $content = preg_replace($re, '<a href="$0" rel="nofollow" target="_blank">$0</a>', $content);

    return $content;
}

function autolink2($content, $username){

	// REGEX to match links
	$re = '/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/'; 

	// EXTRACT links from profile text    
	preg_match_all($re, $content, $links);

	// For each link in profile, add anchor tags
	$fields_num = count($links[0]);
	
	for($i=0; $i<$fields_num; $i++) {
		$links[$i] = preg_replace($re, '<a href="$0" rel="nofollow" target="_blank">$0</a>', $links[$i]);

	}

	// RETURN anchored links
	for($i=0; $i<$fields_num; $i++) {
		echo '<span><a href="index.php?user=';
		echo $username; 
		echo '">';
		echo $username;
		echo '</a>';
		echo ' Links to: ';
		echo "</span>";
		echo $links[0][$i];

		echo "<br>";
		echo "<br>";
		}
}

?>