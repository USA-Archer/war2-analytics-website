<!DOCTYPE html >
<html>
<head>
<link rel="stylesheet" type="text/css" href="reset.css?ver=1">
<link rel="stylesheet" type="text/css" href="vanilla.css?ver=1">
<link rel="stylesheet" type="text/css" href="war2co.css?ver=87">
<link rel="apple-touch-icon" sizes="57x57" href="/ico/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="/ico/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="/ico/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="/ico/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="/ico/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="/ico/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="/ico/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="/ico/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="/ico/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="/ico/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="/ico/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="/ico/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="/ico/favicon-16x16.png">
<link rel="manifest" href="/ico/manifest.json">
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script type="text/javascript">



$(function() {
        var addPlayer = $('#addPlayer');
        var p_players = $('#p_players');
        var i = $('#p_players p').size() + 1;
        
     $('h2').on('click', 'a#addPlayer',  function(e)  {
e.preventDefault();

        				if (i < 9) {
                    $('<p class="row' + i + '"><input name="p' + i + '_user" type="text" id="user_search" placeholder="Player ' + i + ' Username..."><select name="p' + i + '_race"><option value="Any">Any</option><option value="Human">Human</option><option value="Orc">Orc</option></select><select name="p' + i + '_outcome"><option value="Any">Any</option><option value="WIN">Win</option><option value="LOSS">Loss</option><option value="DISC">Disc</option><option value="DRAW">Draw</option></select></p>').appendTo(p_players);
                i++;
               
                }  else if (i = 9) {
                document.getElementById("warning").innerHTML = '<p style="color: red">Max 8 Players</p>';
                }

         
        });
             
$(document).on('click', 'a#remPlayer', function(e)  { 
 
  e.preventDefault();
                if( i > 2 ) {
                        $('p.row' + (i - 1)).remove();
                  
                        i--;
                      
                             
                        }
            
        });
	$( "#datepicker" ).datepicker( {
    dateFormat: 'yy-m-d',
	});


    $( "#datepicker2" ).datepicker( {
    dateFormat: 'yy-m-d',
	});
});


</script>
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="/ico/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
<title>Warcraft 2 Analytics | War2.Co</title>
</head>
   
<body>
<h1><a href="http://war2.co">War2.Co</a>: Warcraft 2 Analytics</h1>

<?php 

$s_page_active = 'no';
$u_page_active = 'no';
$m_page_active = 'no';
$gs_page_active = 'yes';
$l_page_active = 'no';


require 'nav.php' ?>



<div class="game_stats_img">
<div class="site_wrapper">
<div class="head_of_page"><h2 class="float_left"> War2.RU > Game Search</h2></div>
</div>
<img src="img/war2.gif">
</div>
	<?php require 'functions.php';?>



	<div class="user_form">
		<div class="site_wrapper">
			<form name="formSearch" method="get" action="search_report.php" enctype="application/x-www-form-urlencoded">

<p>Time Filter</p>
<p>From<br><input type="text" id="datepicker" name="from" placeholder="Any" value="2010-01-01"></p>
<p>To<br><input type="text" id="datepicker2" name="to" placeholder="Any" value="2020-01-01"></p>
<p>Map Filter</p>
<input name="map" type="text" id="user_search" placeholder="Map..." value="Any">  


  <select name="players">
    <option value="Any">Any</option>
    <option value="1">1</option>
    <option value="2">2</option>
    <option value="3">3</option>
    <option value="4">4</option>
    <option value="5">5</option>
    <option value="6">6</option>
    <option value="7">7</option>
    <option value="8">8</option>
  </select>

<br>
<br>
<p>Player Filter</p>


<h2><a href="#" id="addPlayer">Add Another Player</a></h2>
<div id="warning"></div>

<div id="p_players">


<p>

<input name="p1_user" type="text" id="user_search" placeholder="Player 1 Username...">

<select name="p1_race">
<option value="Any">Any</option>
<option value="Human">Human</option>
<option value="Orc">Orc</option>
</select>

<select name="p1_outcome"><option value="Any">Any</option><option value="WIN">Win</option><option value="LOSS">Loss</option><option value="DISC">Disc</option><option value="DRAW">Draw</option>
</select>

</p>

</div>
<a href="#" id="remPlayer" style="color: red"> X</a>





				<input type="submit" value="Search" id="user_search_button">

			</form>
	
		</div>

	

	</body>
</html>
