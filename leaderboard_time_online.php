<div class="leaderboard_time_online">
<?php

// Select leaderboard time online from user_stats DB //

$query = "SELECT * FROM user_stats ORDER BY total_online_min DESC LIMIT 10";
$stmt = $dbh->prepare($query);
$stmt->execute();

// Fetch all of the remaining rows in the result set //

$result = $stmt->fetchAll();

// Display results from user_stats //

echo "<h3>Total Time Online</h3>";
?>
<table>
<tbody>
<?php

$i = 0;

foreach( $result as $row ) {

        $i = $i + 1;
	$username = $row["username"];
	$min_online = $row["total_online_min"];
	echo '<tr>';
	echo '<td>';
	echo addOrdinalNumberSuffix($i);
	echo '</td>';
	echo '<td>';

	echo '<span><a href="user.php?user=';
	echo $username; 
	echo '">';
	echo $username;
	echo '</a>';
	echo "</span>";
	echo '</td>';
	echo '<td>';
	echo '<span> ';
	echo minToTime($min_online);
	echo '</span>';
	echo '</td>';
	echo '</tr>';

}

?>
</tbody>
</table>
</div>