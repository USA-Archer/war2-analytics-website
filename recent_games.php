<div class="recent_games_inner">
<?php

// Select leaderboard time online from user_stats DB //

$query = "SELECT * FROM game_info ORDER BY ID DESC LIMIT 50";
$stmt = $dbh->prepare($query);
$stmt->execute();

// Fetch all of the remaining rows in the result set //

$result = $stmt->fetchAll();

// Display results from user_stats //


?>
<table>
<tbody>

<?php


echo '<tr>';
echo '<th>Name</th>';
echo '<th>Time</th>';
echo '<th>Map</th>';
echo '<th>Players</th>';
echo '</tr>';
echo '<tr>';



foreach( $result as $row ) {

	$name = $row["name"];
	$time = $row["gametime"];
	$map = $row["mapfile"];
	$id = $row["id"];
	$players = $row["players"];
	


	echo '<td>';
	echo '<span><a href="game_report.php?id=';
	echo $id; 
	echo '">';
	echo $name;
	echo '</a>';
	echo "</span>";
	echo '</td>';

	echo '<td>';
	echo sprintf('<span class="utcdt">%s</span></span>', str_replace('-','/',$time));
	echo '</td>';

	echo '<td>';
	echo $map;
	echo '</td>';
	
	echo '<td>';
	echo $players;
	echo '</td>';

	echo '</tr>';

}

?>
</tbody>
</table>
</div>